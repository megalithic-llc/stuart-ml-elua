#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart-ml.util"


/* name: M.mosesPatchedRange
 * function(...) */
int stuartml_util_mosesPatchedRange (lua_State * L) {
  enum { lc_nformalargs = 0 };
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local arg = {...} */
  lua_createtable(L,1,0);
  const int lc1 = lua_gettop(L);
  {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
  while ((lua_gettop(L) > lc1)) {
    lua_rawseti(L,lc1,(0 + (lua_gettop(L) - lc1)));
  }
  assert(lua_gettop(L) - lc_nextra == 1);

  /* local _start,_stop,_step */
  lua_settop(L,(lua_gettop(L) + 3));
  assert(lua_gettop(L) - lc_nextra == 4);

  /* if #arg==0 then */
  enum { lc2 = 4 };
  const double lc3 = lua_objlen(L,(1 + lc_nextra));
  lua_pushnumber(L,lc3);
  lua_pushnumber(L,0);
  const int lc4 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc4);
  const int lc5 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc5) {

    /* return {} */
    lua_newtable(L);
    return 1;
    assert(lua_gettop(L) - lc_nextra == 4);
  }
  else {

    /* elseif #arg==1 then */
    enum { lc6 = 4 };
    const double lc7 = lua_objlen(L,(1 + lc_nextra));
    lua_pushnumber(L,lc7);
    lua_pushnumber(L,1);
    const int lc8 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc8);
    const int lc9 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc9) {

      /* _stop,_start,_step = arg[1],0,1 */
      lua_pushnumber(L,1);
      lua_gettable(L,(1 + lc_nextra));
      lua_pushnumber(L,0);
      lua_pushnumber(L,1);
      lua_replace(L,(4 + lc_nextra));
      lua_replace(L,(2 + lc_nextra));
      lua_replace(L,(3 + lc_nextra));
      assert(lua_gettop(L) - lc_nextra == 4);
    }
    else {

      /* elseif #arg==2 then */
      enum { lc10 = 4 };
      const double lc11 = lua_objlen(L,(1 + lc_nextra));
      lua_pushnumber(L,lc11);
      lua_pushnumber(L,2);
      const int lc12 = lua_equal(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc12);
      const int lc13 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc13) {

        /* _start,_stop,_step = arg[1],arg[2],1 */
        lua_pushnumber(L,1);
        lua_gettable(L,(1 + lc_nextra));
        lua_pushnumber(L,2);
        lua_gettable(L,(1 + lc_nextra));
        lua_pushnumber(L,1);
        lua_replace(L,(4 + lc_nextra));
        lua_replace(L,(3 + lc_nextra));
        lua_replace(L,(2 + lc_nextra));
        assert(lua_gettop(L) - lc_nextra == 4);
      }
      else {

        /* elseif #arg == 3 then */
        enum { lc14 = 4 };
        const double lc15 = lua_objlen(L,(1 + lc_nextra));
        lua_pushnumber(L,lc15);
        lua_pushnumber(L,3);
        const int lc16 = lua_equal(L,-2,-1);
        lua_pop(L,2);
        lua_pushboolean(L,lc16);
        const int lc17 = lua_toboolean(L,-1);
        lua_pop(L,1);
        if (lc17) {

          /* _start,_stop,_step = arg[1],arg[2],arg[3] */
          lua_pushnumber(L,1);
          lua_gettable(L,(1 + lc_nextra));
          lua_pushnumber(L,2);
          lua_gettable(L,(1 + lc_nextra));
          lua_pushnumber(L,3);
          lua_gettable(L,(1 + lc_nextra));
          lua_replace(L,(4 + lc_nextra));
          lua_replace(L,(3 + lc_nextra));
          lua_replace(L,(2 + lc_nextra));
          assert(lua_gettop(L) - lc_nextra == 4);
        }
        lua_settop(L,(lc14 + lc_nextra));
      }
      lua_settop(L,(lc10 + lc_nextra));
    }
    lua_settop(L,(lc6 + lc_nextra));
  }
  lua_settop(L,(lc2 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 4);

  /* if (_step and _step==0) then */
  enum { lc18 = 4 };
  lua_pushvalue(L,(4 + lc_nextra));
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,(4 + lc_nextra));
    lua_pushnumber(L,0);
    const int lc19 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc19);
  }
  const int lc20 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc20) {

    /* return {} */
    lua_newtable(L);
    return 1;
    assert(lua_gettop(L) - lc_nextra == 4);
  }
  lua_settop(L,(lc18 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 4);

  /* -- BEGIN patch --------------------------------------------------------------
   * if _start == 1 and _stop == 1 and _step == 1 then */
  enum { lc21 = 4 };
  lua_pushvalue(L,(2 + lc_nextra));
  lua_pushnumber(L,1);
  const int lc22 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc22);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,(3 + lc_nextra));
    lua_pushnumber(L,1);
    const int lc23 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc23);
  }
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,(4 + lc_nextra));
    lua_pushnumber(L,1);
    const int lc24 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc24);
  }
  const int lc25 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc25) {

    /* return {1} */
    lua_createtable(L,1,0);
    lua_pushnumber(L,1);
    lua_rawseti(L,-2,1);
    return 1;
    assert(lua_gettop(L) - lc_nextra == 4);
  }
  lua_settop(L,(lc21 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 4);

  /* -- END patch ----------------------------------------------------------------
   * local _ranged = {} */
  lua_newtable(L);
  assert(lua_gettop(L) - lc_nextra == 5);

  /* local _steps = math.max(math.floor((_stop-_start)/_step),0) */
  lua_getfield(L,LUA_ENVIRONINDEX,"math");
  lua_pushliteral(L,"max");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_getfield(L,LUA_ENVIRONINDEX,"math");
  lua_pushliteral(L,"floor");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_sub(L,(3 + lc_nextra),(2 + lc_nextra));
  lc_div(L,-1,(4 + lc_nextra));
  lua_remove(L,-2);
  lua_call(L,1,1);
  lua_pushnumber(L,0);
  lua_call(L,2,1);
  assert(lua_gettop(L) - lc_nextra == 6);

  /* for i=1,_steps do */
  lua_pushnumber(L,1);
  if (!((lua_isnumber(L,-1) && lua_isnumber(L,(6 + lc_nextra))))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc26_var = lua_tonumber(L,-1);
  const double lc27_limit = lua_tonumber(L,(6 + lc_nextra));
  const double lc28_step = 1;
  lua_pop(L,1);
  enum { lc29 = 6 };
  while ((((lc28_step > 0) && (lc26_var <= lc27_limit)) || ((lc28_step <= 0) && (lc26_var >= lc27_limit)))) {

    /* internal: local i at index 7 */
    lua_pushnumber(L,lc26_var);

    /* _ranged[#_ranged+1] = _start+_step*i */
    lc_mul(L,(4 + lc_nextra),(7 + lc_nextra));
    lc_add(L,(2 + lc_nextra),-1);
    lua_remove(L,-2);
    const double lc30 = lua_objlen(L,(5 + lc_nextra));
    lua_pushnumber(L,lc30);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,(5 + lc_nextra));
    assert(lua_gettop(L) - lc_nextra == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc26_var += lc28_step;
  }
  lua_settop(L,(lc29 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 6);

  /* if #_ranged>0 then */
  enum { lc31 = 6 };
  lua_pushnumber(L,0);
  const double lc32 = lua_objlen(L,(5 + lc_nextra));
  lua_pushnumber(L,lc32);
  const int lc33 = lua_lessthan(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc33);
  const int lc34 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc34) {

    /* table.insert(_ranged,1,_start) */
    lua_getfield(L,LUA_ENVIRONINDEX,"table");
    lua_pushliteral(L,"insert");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,(5 + lc_nextra));
    lua_pushnumber(L,1);
    lua_pushvalue(L,(2 + lc_nextra));
    lua_call(L,3,0);
    assert(lua_gettop(L) - lc_nextra == 6);
  }
  lua_settop(L,(lc31 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 6);

  /* return _ranged */
  lua_pushvalue(L,(5 + lc_nextra));
  return 1;
  assert(lua_gettop(L) - lc_nextra == 6);
}


/* function(array) */
int stuartml_util_mosesPatchedZip_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return #array */
  const double lc36 = lua_objlen(L,1);
  lua_pushnumber(L,lc36);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: M.mosesPatchedZip
 * function(...) */
int stuartml_util_mosesPatchedZip (lua_State * L) {
  lua_checkstack(L,20);
  enum { lc_nformalargs = 0 };
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) - lc_nextra == 1);

  /* local args = {...} */
  lua_createtable(L,1,0);
  const int lc35 = lua_gettop(L);
  {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
  while ((lua_gettop(L) > lc35)) {
    lua_rawseti(L,lc35,(0 + (lua_gettop(L) - lc35)));
  }
  assert(lua_gettop(L) - lc_nextra == 2);

  /* local n = moses.max(args, function(array) return #array end) */
  lua_pushliteral(L,"max");
  lua_gettable(L,(1 + lc_nextra));
  lua_pushvalue(L,(2 + lc_nextra));
  lua_pushcfunction(L,stuartml_util_mosesPatchedZip_1);
  lua_call(L,2,1);
  assert(lua_gettop(L) - lc_nextra == 3);

  /* local _ans = {} */
  lua_newtable(L);
  assert(lua_gettop(L) - lc_nextra == 4);

  /* for i = 1,n do */
  lua_pushnumber(L,1);
  if (!((lua_isnumber(L,-1) && lua_isnumber(L,(3 + lc_nextra))))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc38_var = lua_tonumber(L,-1);
  const double lc39_limit = lua_tonumber(L,(3 + lc_nextra));
  const double lc40_step = 1;
  lua_pop(L,1);
  enum { lc41 = 4 };
  while ((((lc40_step > 0) && (lc38_var <= lc39_limit)) || ((lc40_step <= 0) && (lc38_var >= lc39_limit)))) {

    /* internal: local i at index 5 */
    lua_pushnumber(L,lc38_var);

    /* if not _ans[i] then */
    enum { lc42 = 5 };
    lua_pushvalue(L,(5 + lc_nextra));
    lua_gettable(L,(4 + lc_nextra));
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
    const int lc43 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc43) {

      /* _ans[i] = {} */
      lua_newtable(L);
      lua_pushvalue(L,(5 + lc_nextra));
      lua_insert(L,-2);
      lua_settable(L,(4 + lc_nextra));
      assert(lua_gettop(L) - lc_nextra == 5);
    }
    lua_settop(L,(lc42 + lc_nextra));
    assert(lua_gettop(L) - lc_nextra == 5);

    /* for k, array in ipairs(args) do
     * internal: local f, s, var = explist */
    enum { lc44 = 5 };
    lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
    lua_pushvalue(L,(2 + lc_nextra));
    lua_call(L,1,3);
    while (1) {

      /* internal: local var_1, ..., var_n = f(s, var)
       *           if var_1 == nil then break end
       *           var = var_1 */
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_call(L,2,2);
      if (lua_isnil(L,-2)) {
        break;
      }
      lua_pushvalue(L,-2);
      lua_replace(L,-4);

      /* internal: local k with idx 9
       * internal: local array with idx 10 */


      /* if (array[i]~=nil) then */
      enum { lc45 = 10 };
      lua_pushvalue(L,(5 + lc_nextra));
      lua_gettable(L,(10 + lc_nextra));
      lua_pushnil(L);
      const int lc46 = lua_equal(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc46);
      lua_pushboolean(L,!(lua_toboolean(L,-1)));
      lua_remove(L,-2);
      const int lc47 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc47) {

        /* _ans[i][#_ans[i]+1] = array[i] */
        lua_pushvalue(L,(5 + lc_nextra));
        lua_gettable(L,(10 + lc_nextra));
        lua_pushvalue(L,(5 + lc_nextra));
        lua_gettable(L,(4 + lc_nextra));
        lua_insert(L,-2);
        lua_pushvalue(L,(5 + lc_nextra));
        lua_gettable(L,(4 + lc_nextra));
        const double lc48 = lua_objlen(L,-1);
        lua_pop(L,1);
        lua_pushnumber(L,lc48);
        lua_pushnumber(L,1);
        lc_add(L,-2,-1);
        lua_remove(L,-2);
        lua_remove(L,-2);
        lua_insert(L,-2);
        lua_settable(L,-3);
        lua_pop(L,1);
        assert(lua_gettop(L) - lc_nextra == 10);
      }
      lua_settop(L,(lc45 + lc_nextra));
      assert(lua_gettop(L) - lc_nextra == 10);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,2);
    }
    lua_settop(L,(lc44 + lc_nextra));
    assert(lua_gettop(L) - lc_nextra == 5);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc38_var += lc40_step;
  }
  lua_settop(L,(lc41 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 4);

  /* return _ans */
  lua_pushvalue(L,(4 + lc_nextra));
  return 1;
  assert(lua_gettop(L) - lc_nextra == 4);
}


/* function() */
int stuartml_util_tableIterator_1 (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* i = i + 1 */
  lc_getupvalue(L,lua_upvalueindex(1),0,2);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_setupvalue(L,lua_upvalueindex(1),0,2);
  assert(lua_gettop(L) == 0);

  /* if i <= #table then */
  enum { lc51 = 0 };
  lc_getupvalue(L,lua_upvalueindex(1),0,2);
  lc_getupvalue(L,lua_upvalueindex(1),1,1);
  const double lc52 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc52);
  const int lc53 = lc_le(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc53);
  const int lc54 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc54) {

    /* return table[i] */
    lc_getupvalue(L,lua_upvalueindex(1),1,1);
    lc_getupvalue(L,lua_upvalueindex(1),0,2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    return 1;
    assert(lua_gettop(L) == 0);
  }
  lua_settop(L,lc51);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.tableIterator
 * function(table) */
int stuartml_util_tableIterator (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc49 = 2 };
  assert((lua_gettop(L) == lc49));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,1);

  /* local i = 0 */
  lc_newclosuretable(L,lc49);
  enum { lc50 = 3 };
  assert((lua_gettop(L) == lc50));
  lua_pushnumber(L,0);
  lua_rawseti(L,lc50,2);
  assert(lua_gettop(L) == 3);

  /* return function()
   *     i = i + 1
   *     if i <= #table then return table[i] end
   *   end */
  lua_pushvalue(L,lc50);
  lua_pushcclosure(L,stuartml_util_tableIterator_1,1);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: M.unzip
 * function(array) */
int stuartml_util_unzip (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local util = require 'stuart-ml.util' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local zip = util.mosesPatchedZip */
  lua_pushliteral(L,"mosesPatchedZip");
  lua_gettable(L,2);
  assert(lua_gettop(L) == 3);

  /* local unpack = table.unpack or unpack */
  lua_getfield(L,LUA_ENVIRONINDEX,"table");
  lua_pushliteral(L,"unpack");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_getfield(L,LUA_ENVIRONINDEX,"unpack");
  }
  assert(lua_gettop(L) == 4);

  /* return zip(unpack(array)) */
  const int lc56 = lua_gettop(L);
  lua_pushvalue(L,3);
  const int lc57 = lua_gettop(L);
  lua_pushvalue(L,4);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc57),LUA_MULTRET);
  return (lua_gettop(L) - lc56);
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_util_map[] = {
  { LSTRKEY("mosesPatchedRange") , LFUNCVAL (stuartml_util_mosesPatchedRange) },
  { LSTRKEY("mosesPatchedZip")   , LFUNCVAL (stuartml_util_mosesPatchedZip) },
  { LSTRKEY("tableIterator")     , LFUNCVAL (stuartml_util_tableIterator) },
  { LSTRKEY("unzip")             , LFUNCVAL (stuartml_util_unzip) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_util( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_UTIL, stuartml_util_map );
  return 1;
#endif
}
