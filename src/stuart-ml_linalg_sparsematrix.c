#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart-ml_linalg_matrix.h"

#define MODULE "stuart-ml.linalg.SparseMatrix"
#define SUPERMODULE "stuart-ml.linalg.Matrix"


/* name: SparseMatrix:__eq
 * function() */
int stuartml_linalg_SparseMatrix___eq (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: SparseMatrix:_init
 * function(numRows, numCols, colPtrs, rowIndices, values, isTransposed) */
int stuartml_linalg_SparseMatrix__init (lua_State * L) {
  enum { lc_nformalargs = 7 };
  lua_settop(L,7);

  /* assert(#values == #rowIndices) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const double lc2 = lua_objlen(L,6);
  lua_pushnumber(L,lc2);
  const double lc3 = lua_objlen(L,5);
  lua_pushnumber(L,lc3);
  const int lc4 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc4);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 7);

  /* -- The number of row indices and values don't match
   * if isTransposed then */
  enum { lc5 = 7 };
  if (lua_toboolean(L,7)) {

    /* assert(#colPtrs == numRows+1) */
    lua_getfield(L,LUA_ENVIRONINDEX,"assert");
    const double lc6 = lua_objlen(L,4);
    lua_pushnumber(L,lc6);
    lua_pushnumber(L,1);
    lc_add(L,2,-1);
    lua_remove(L,-2);
    const int lc7 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc7);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 7);
  }
  else {

    /* else
     * assert(#colPtrs == numCols+1) */
    lua_getfield(L,LUA_ENVIRONINDEX,"assert");
    const double lc8 = lua_objlen(L,4);
    lua_pushnumber(L,lc8);
    lua_pushnumber(L,1);
    lc_add(L,3,-1);
    lua_remove(L,-2);
    const int lc9 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc9);
    lua_call(L,1,0);
    assert(lua_gettop(L) == 7);
  }
  lua_settop(L,lc5);
  assert(lua_gettop(L) == 7);

  /* assert(#values == colPtrs[#colPtrs]) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const double lc10 = lua_objlen(L,6);
  lua_pushnumber(L,lc10);
  const double lc11 = lua_objlen(L,4);
  lua_pushnumber(L,lc11);
  lua_gettable(L,4);
  const int lc12 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc12);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 7);

  /* -- The last value of colPtrs must equal the number of elements
   * local Matrix = require 'stuart-ml.linalg.Matrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,SUPERMODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* Matrix._init(self) */
  lua_pushliteral(L,"_init");
  lua_gettable(L,8);
  lua_pushvalue(L,1);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 8);

  /* self.numRows = numRows */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"numRows");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 8);

  /* self.numCols = numCols */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"numCols");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 8);

  /* self.colPtrs = colPtrs */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"colPtrs");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 8);

  /* self.rowIndices = rowIndices */
  lua_pushvalue(L,5);
  lua_pushliteral(L,"rowIndices");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 8);

  /* self.values = values */
  lua_pushvalue(L,6);
  lua_pushliteral(L,"values");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 8);

  /* self.isTransposed = isTransposed or false */
  lua_pushvalue(L,7);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushboolean(L,0);
  }
  lua_pushliteral(L,"isTransposed");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 8);
  return 0;
}


/* name: SparseMatrix:asBreeze
 * function() */
int stuartml_linalg_SparseMatrix_asBreeze (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: SparseMatrix:asML
 * function() */
int stuartml_linalg_SparseMatrix_asML (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: SparseMatrix:colIter
 * function() */
int stuartml_linalg_SparseMatrix_colIter (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: SparseMatrix:copy
 * function() */
int stuartml_linalg_SparseMatrix_copy (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuartml_linalg_SparseMatrix_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: SparseMatrix:foreachActive
 * function(f) */
int stuartml_linalg_SparseMatrix_foreachActive (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if not self.isTransposed then */
  enum { lc13 = 2 };
  lua_pushliteral(L,"isTransposed");
  lua_gettable(L,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc14 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc14) {

    /* for j = 0, self.numCols-1 do */
    lua_pushnumber(L,0);
    lua_pushliteral(L,"numCols");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_sub(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc15_var = lua_tonumber(L,-2);
    const double lc16_limit = lua_tonumber(L,-1);
    const double lc17_step = 1;
    lua_pop(L,2);
    enum { lc18 = 2 };
    while ((((lc17_step > 0) && (lc15_var <= lc16_limit)) || ((lc17_step <= 0) && (lc15_var >= lc16_limit)))) {

      /* internal: local j at index 3 */
      lua_pushnumber(L,lc15_var);

      /* local idx = self.colPtrs[j+1] */
      lua_pushliteral(L,"colPtrs");
      lua_gettable(L,1);
      lua_pushnumber(L,1);
      lc_add(L,3,-1);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      assert(lua_gettop(L) == 4);

      /* local idxEnd = self.colPtrs[j + 2] */
      lua_pushliteral(L,"colPtrs");
      lua_gettable(L,1);
      lua_pushnumber(L,2);
      lc_add(L,3,-1);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      assert(lua_gettop(L) == 5);

      /* while idx < idxEnd do */
      enum { lc19 = 5 };
      while (1) {
        const int lc20 = lua_lessthan(L,4,5);
        lua_pushboolean(L,lc20);
        if (!(lua_toboolean(L,-1))) {
          break;
        }
        lua_pop(L,1);

        /* f(self.rowIndices[idx+1], j, self.values[idx+1]) */
        lua_pushvalue(L,2);
        lua_pushliteral(L,"rowIndices");
        lua_gettable(L,1);
        lua_pushnumber(L,1);
        lc_add(L,4,-1);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        lua_pushvalue(L,3);
        lua_pushliteral(L,"values");
        lua_gettable(L,1);
        lua_pushnumber(L,1);
        lc_add(L,4,-1);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        lua_call(L,3,0);
        assert(lua_gettop(L) == 5);

        /* idx = idx + 1 */
        lua_pushnumber(L,1);
        lc_add(L,4,-1);
        lua_remove(L,-2);
        lua_replace(L,4);
        assert(lua_gettop(L) == 5);
      }
      lua_settop(L,lc19);
      assert(lua_gettop(L) == 5);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,3);
      lc15_var += lc17_step;
    }
    lua_settop(L,lc18);
    assert(lua_gettop(L) == 2);
  }
  else {

    /* else
     * for i = 0, self.numRows-1 do */
    lua_pushnumber(L,0);
    lua_pushliteral(L,"numRows");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_sub(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc21_var = lua_tonumber(L,-2);
    const double lc22_limit = lua_tonumber(L,-1);
    const double lc23_step = 1;
    lua_pop(L,2);
    enum { lc24 = 2 };
    while ((((lc23_step > 0) && (lc21_var <= lc22_limit)) || ((lc23_step <= 0) && (lc21_var >= lc22_limit)))) {

      /* internal: local i at index 3 */
      lua_pushnumber(L,lc21_var);

      /* local idx = self.colPtrs[i+1] */
      lua_pushliteral(L,"colPtrs");
      lua_gettable(L,1);
      lua_pushnumber(L,1);
      lc_add(L,3,-1);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      assert(lua_gettop(L) == 4);

      /* local idxEnd = self.colPtrs[i + 2] */
      lua_pushliteral(L,"colPtrs");
      lua_gettable(L,1);
      lua_pushnumber(L,2);
      lc_add(L,3,-1);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      assert(lua_gettop(L) == 5);

      /* while idx < idxEnd do */
      enum { lc25 = 5 };
      while (1) {
        const int lc26 = lua_lessthan(L,4,5);
        lua_pushboolean(L,lc26);
        if (!(lua_toboolean(L,-1))) {
          break;
        }
        lua_pop(L,1);

        /* local j = self.rowIndices[idx+1] */
        lua_pushliteral(L,"rowIndices");
        lua_gettable(L,1);
        lua_pushnumber(L,1);
        lc_add(L,4,-1);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        assert(lua_gettop(L) == 6);

        /* f(i, j, self.values[idx+1]) */
        lua_pushvalue(L,2);
        lua_pushvalue(L,3);
        lua_pushvalue(L,6);
        lua_pushliteral(L,"values");
        lua_gettable(L,1);
        lua_pushnumber(L,1);
        lc_add(L,4,-1);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        lua_call(L,3,0);
        assert(lua_gettop(L) == 6);

        /* idx = idx + 1 */
        lua_pushnumber(L,1);
        lc_add(L,4,-1);
        lua_remove(L,-2);
        lua_replace(L,4);
        assert(lua_gettop(L) == 6);

        /* internal: stack cleanup on scope exit */
        lua_pop(L,1);
      }
      lua_settop(L,lc25);
      assert(lua_gettop(L) == 5);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,3);
      lc21_var += lc23_step;
    }
    lua_settop(L,lc24);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc13);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: SparseMatrix.fromCOO
 * function() */
int stuartml_linalg_SparseMatrix_fromCOO (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: SparseMatrix.fromML
 * function() */
int stuartml_linalg_SparseMatrix_fromML (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: SparseMatrix.genRandMatrix
 * function() */
int stuartml_linalg_SparseMatrix_genRandMatrix (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: SparseMatrix:get
 * function(i, j) */
int stuartml_linalg_SparseMatrix_get (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local ind = self:index(i, j) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"index");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_pushvalue(L,3);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 4);

  /* if ind < 1 then */
  enum { lc27 = 4 };
  lua_pushnumber(L,1);
  const int lc28 = lua_lessthan(L,4,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc28);
  const int lc29 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc29) {

    /* return 0.0 */
    lua_pushnumber(L,0);
    return 1;
    assert(lua_gettop(L) == 4);
  }
  else {

    /* else
     * return self.values[ind] */
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushvalue(L,4);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    return 1;
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc27);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* name: SparseMatrix:index
 * function(i, j) */
int stuartml_linalg_SparseMatrix_index (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* assert(i >= 0 and i < self.numRows) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnumber(L,0);
  const int lc30 = lc_le(L,-1,2);
  lua_pop(L,1);
  lua_pushboolean(L,lc30);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"numRows");
    lua_gettable(L,1);
    const int lc31 = lua_lessthan(L,2,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc31);
  }
  lua_call(L,1,0);
  assert(lua_gettop(L) == 3);

  /* assert(j >= 0 and j < self.numCols) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnumber(L,0);
  const int lc32 = lc_le(L,-1,3);
  lua_pop(L,1);
  lua_pushboolean(L,lc32);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"numCols");
    lua_gettable(L,1);
    const int lc33 = lua_lessthan(L,3,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc33);
  }
  lua_call(L,1,0);
  assert(lua_gettop(L) == 3);

  /* local arrays = require 'stuart-ml.util.java.arrays' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.util.java.arrays");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* if not self.isTransposed then */
  enum { lc34 = 4 };
  lua_pushliteral(L,"isTransposed");
  lua_gettable(L,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc35 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc35) {

    /* return arrays.binarySearch(self.rowIndices, self.colPtrs[j], self.colPtrs[j+1], i) */
    const int lc36 = lua_gettop(L);
    lua_pushliteral(L,"binarySearch");
    lua_gettable(L,4);
    lua_pushliteral(L,"rowIndices");
    lua_gettable(L,1);
    lua_pushliteral(L,"colPtrs");
    lua_gettable(L,1);
    lua_pushvalue(L,3);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"colPtrs");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_add(L,3,-1);
    lua_remove(L,-2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,2);
    lua_call(L,4,LUA_MULTRET);
    return (lua_gettop(L) - lc36);
    assert(lua_gettop(L) == 4);
  }
  else {

    /* else
     * return arrays.binarySearch(self.rowIndices, self.colPtrs[i], self.colPtrs[i+1], j) */
    const int lc37 = lua_gettop(L);
    lua_pushliteral(L,"binarySearch");
    lua_gettable(L,4);
    lua_pushliteral(L,"rowIndices");
    lua_gettable(L,1);
    lua_pushliteral(L,"colPtrs");
    lua_gettable(L,1);
    lua_pushvalue(L,2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"colPtrs");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_add(L,2,-1);
    lua_remove(L,-2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,3);
    lua_call(L,4,LUA_MULTRET);
    return (lua_gettop(L) - lc37);
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc34);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* name: SparseMatrix:map
 * function() */
int stuartml_linalg_SparseMatrix_map (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local SparseMatrix = require 'stuart-ml.linalg.SparseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return SparseMatrix.new(self.numRows, self.numCols, self.colPtrs, self.rowIndices, moses.map(self.values, f), self.isTransposed) */
  const int lc38 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,1);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,1);
  lua_pushliteral(L,"colPtrs");
  lua_gettable(L,1);
  lua_pushliteral(L,"rowIndices");
  lua_gettable(L,1);
  lua_pushliteral(L,"map");
  lua_gettable(L,3);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  lua_pushliteral(L,"isTransposed");
  lua_gettable(L,1);
  lua_call(L,6,LUA_MULTRET);
  return (lua_gettop(L) - lc38);
  assert(lua_gettop(L) == 4);
}


/* name: M.new
 * function(...) */
int stuartml_linalg_SparseMatrix_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


/* name: SparseMatrix:numActives
 * function() */
int stuartml_linalg_SparseMatrix_numActives (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return #self.values */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  const double lc38 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc38);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(x) */
int stuartml_linalg_SparseMatrix_numNonzeros_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return x ~= 0 */
  lua_pushnumber(L,0);
  const int lc40 = lua_equal(L,1,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc40);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: SparseMatrix:numNonzeros
 * function() */
int stuartml_linalg_SparseMatrix_numNonzeros (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return moses.countf(self.values, function(x) return x ~= 0 end) */
  const int lc39 = lua_gettop(L);
  lua_pushliteral(L,"countf");
  lua_gettable(L,2);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushcfunction(L,stuartml_linalg_SparseMatrix_numNonzeros_1);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc39);
  assert(lua_gettop(L) == 2);
}


/* name: SparseMatrix.spdiag
 * function() */
int stuartml_linalg_SparseMatrix_spdiag (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: SparseMatrix.speye
 * function() */
int stuartml_linalg_SparseMatrix_speye (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: SparseMatrix.sprand
 * function() */
int stuartml_linalg_SparseMatrix_sprand (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: SparseMatrix.sprandn
 * function() */
int stuartml_linalg_SparseMatrix_sprandn (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: SparseMatrix:toDense
 * function() */
int stuartml_linalg_SparseMatrix_toDense (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return DenseMatrix.new(self.numRows, self.numCols, self:toArray()) */
  const int lc42 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,2);
  const int lc43 = lua_gettop(L);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,1);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,1);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toArray");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc43),LUA_MULTRET);
  return (lua_gettop(L) - lc42);
  assert(lua_gettop(L) == 2);
}


/* name: SparseMatrix:toSparse
 * function() */
int stuartml_linalg_SparseMatrix_toSparse (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: SparseMatrix:transpose
 * function() */
int stuartml_linalg_SparseMatrix_transpose (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local SparseMatrix = require 'stuart-ml.linalg.SparseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return SparseMatrix.new(self.numCols, self.numRows, self.colPtrs, self.rowIndices, self.values, not self.isTransposed) */
  const int lc45 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,2);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,1);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,1);
  lua_pushliteral(L,"colPtrs");
  lua_gettable(L,1);
  lua_pushliteral(L,"rowIndices");
  lua_gettable(L,1);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushliteral(L,"isTransposed");
  lua_gettable(L,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  lua_call(L,6,LUA_MULTRET);
  return (lua_gettop(L) - lc45);
  assert(lua_gettop(L) == 2);
}


/* name: SparseMatrix:update
 * function(i, j, v) */
int stuartml_linalg_SparseMatrix_update (lua_State * L) {
  enum { lc_nformalargs = 1 };
  if ((lua_gettop(L) < lc_nformalargs)) {
    lua_settop(L,lc_nformalargs);
  }
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) - lc_nextra == 2);

  /* local nargs = #moses.pack(...) */
  lua_pushliteral(L,"pack");
  lua_gettable(L,(2 + lc_nextra));
  const int lc46 = lua_gettop(L);
  {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
  lua_call(L,(lua_gettop(L) - lc46),1);
  const double lc47 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc47);
  assert(lua_gettop(L) - lc_nextra == 3);

  /* if nargs == 1 then */
  enum { lc48 = 3 };
  lua_pushnumber(L,1);
  const int lc49 = lua_equal(L,(3 + lc_nextra),-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc49);
  const int lc50 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc50) {

    /* return self:updatef(...) */
    const int lc51 = lua_gettop(L);
    lua_pushvalue(L,1);
    const int lc52 = lua_gettop(L);
    lua_pushliteral(L,"updatef");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc52),LUA_MULTRET);
    return (lua_gettop(L) - lc51);
    assert(lua_gettop(L) - lc_nextra == 3);
  }
  else {

    /* else
     * return self:update3(...) */
    const int lc53 = lua_gettop(L);
    lua_pushvalue(L,1);
    const int lc54 = lua_gettop(L);
    lua_pushliteral(L,"update3");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc54),LUA_MULTRET);
    return (lua_gettop(L) - lc53);
    assert(lua_gettop(L) - lc_nextra == 3);
  }
  lua_settop(L,(lc48 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 3);
  return 0;
}


/* name: SparseMatrix:updatef
 * function(f) */
int stuartml_linalg_SparseMatrix_updatef (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for i=1,#self.values do */
  lua_pushnumber(L,1);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  const double lc58 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc58);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc55_var = lua_tonumber(L,-2);
  const double lc56_limit = lua_tonumber(L,-1);
  const double lc57_step = 1;
  lua_pop(L,2);
  enum { lc59 = 2 };
  while ((((lc57_step > 0) && (lc55_var <= lc56_limit)) || ((lc57_step <= 0) && (lc55_var >= lc56_limit)))) {

    /* internal: local i at index 3 */
    lua_pushnumber(L,lc55_var);

    /* self.values[i] = f(self.values[i]) */
    lua_pushvalue(L,2);
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushvalue(L,3);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_call(L,1,1);
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_insert(L,-2);
    lua_pushvalue(L,3);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 3);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc55_var += lc57_step;
  }
  lua_settop(L,lc59);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: SparseMatrix:update3
 * function(i, j, v) */
int stuartml_linalg_SparseMatrix_update3 (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);

  /* local ind = self:index(i, j) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"index");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_pushvalue(L,3);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 5);

  /* assert(ind >= 1) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnumber(L,1);
  const int lc60 = lc_le(L,-1,5);
  lua_pop(L,1);
  lua_pushboolean(L,lc60);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 5);

  /* self.values[ind] = v */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushvalue(L,5);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 5);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgsmat_map[] = {
  { LSTRKEY("__eq")            , LFUNCVAL (stuartml_linalg_SparseMatrix___eq) },
  { LSTRKEY("_init")           , LFUNCVAL (stuartml_linalg_SparseMatrix__init) },
  { LSTRKEY("asBreeze")        , LFUNCVAL (stuartml_linalg_SparseMatrix_asBreeze) },
  { LSTRKEY("asML")            , LFUNCVAL (stuartml_linalg_SparseMatrix_asML) },
  { LSTRKEY("colIter")         , LFUNCVAL (stuartml_linalg_SparseMatrix_colIter) },
  { LSTRKEY("copy")            , LFUNCVAL (stuartml_linalg_SparseMatrix_copy) },
  { LSTRKEY("foreachActive")   , LFUNCVAL (stuartml_linalg_SparseMatrix_foreachActive) },
  { LSTRKEY("fromCOO")         , LFUNCVAL (stuartml_linalg_SparseMatrix_fromCOO) },
  { LSTRKEY("fromML")          , LFUNCVAL (stuartml_linalg_SparseMatrix_fromML) },
  { LSTRKEY("genRandMatrix")   , LFUNCVAL (stuartml_linalg_SparseMatrix_genRandMatrix) },
  { LSTRKEY("get")             , LFUNCVAL (stuartml_linalg_SparseMatrix_get) },
  { LSTRKEY("index")           , LFUNCVAL (stuartml_linalg_SparseMatrix_index) },
  { LSTRKEY("map")             , LFUNCVAL (stuartml_linalg_SparseMatrix_map) },
  { LSTRKEY("numActives")      , LFUNCVAL (stuartml_linalg_SparseMatrix_numActives) },
  { LSTRKEY("numNonzeros")     , LFUNCVAL (stuartml_linalg_SparseMatrix_numNonzeros) },
  { LSTRKEY("spdiag")          , LFUNCVAL (stuartml_linalg_SparseMatrix_spdiag) },
  { LSTRKEY("speye")           , LFUNCVAL (stuartml_linalg_SparseMatrix_speye) },
  { LSTRKEY("sprand")          , LFUNCVAL (stuartml_linalg_SparseMatrix_sprand) },
  { LSTRKEY("sprandn")         , LFUNCVAL (stuartml_linalg_SparseMatrix_sprandn) },
  { LSTRKEY("toDense")         , LFUNCVAL (stuartml_linalg_SparseMatrix_toDense) },
  { LSTRKEY("toSparse")        , LFUNCVAL (stuartml_linalg_SparseMatrix_toSparse) },
  { LSTRKEY("transpose")       , LFUNCVAL (stuartml_linalg_SparseMatrix_transpose) },
  { LSTRKEY("update")          , LFUNCVAL (stuartml_linalg_SparseMatrix_update) },
  { LSTRKEY("updatef")         , LFUNCVAL (stuartml_linalg_SparseMatrix_updatef) },
  { LSTRKEY("update3")         , LFUNCVAL (stuartml_linalg_SparseMatrix_update3) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuartml_linalgsmat_map) },
  { LSTRKEY("_base")           , LRO_ROVAL(stuartml_linalgmat_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuartml_linalgsmat_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuartml_linalg_SparseMatrix_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuartml_linalg_SparseMatrix_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("multiply")        , LFUNCVAL (stuartml_linalg_Matrix_multiply) },
  { LSTRKEY("toArray")         , LFUNCVAL (stuartml_linalg_Matrix_toArray) },
  { LSTRKEY("toString")        , LFUNCVAL (stuartml_linalg_Matrix_toString) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgsmat( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGSMAT, stuartml_linalgsmat_map );
  return 1;
#endif
}
