#ifndef __STUARTML_LINALG_VECTOR_H__
#define __STUARTML_LINALG_VECTOR_H__

#include "lua.h"

extern LUA_REG_TYPE stuartml_linalgvec_map[];

int stuartml_linalg_Vector__init (lua_State * L);
int stuartml_linalg_Vector_numActives (lua_State * L);
int stuartml_linalg_Vector_numNonzeros (lua_State * L);

#endif
