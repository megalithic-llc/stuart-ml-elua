#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart-ml_linalg_vector.h"

#define MODULE "stuart-ml.linalg.DenseVector"
#define SUPERMODULE "stuart-ml.linalg.Vector"


/* name: DenseVector.__eq
 * function(a, b) */
int stuartml_linalg_DenseVector___eq (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if a:size() ~= b:size() then */
  enum { lc2 = 2 };
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  const int lc3 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc3);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc4 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc4) {

    /* return false */
    lua_pushboolean(L,0);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc2);
  assert(lua_gettop(L) == 2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return moses.same(a.values, b.values) */
  const int lc5 = lua_gettop(L);
  lua_pushliteral(L,"same");
  lua_gettable(L,3);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushliteral(L,"values");
  lua_gettable(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc5);
  assert(lua_gettop(L) == 3);
}


/* name: DenseVector:__index
 * function(key) */
int stuartml_linalg_DenseVector___index (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if type(key)~='number' then */
  enum { lc6 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"type");
  lua_pushvalue(L,2);
  lua_call(L,1,1);
  lua_pushliteral(L,"number");
  const int lc7 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc7);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc8 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc8) {

    /* return rawget(getmetatable(self), key) */
    const int lc9 = lua_gettop(L);
    lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
    lua_getfield(L,LUA_ENVIRONINDEX,"getmetatable");
    lua_pushvalue(L,1);
    lua_call(L,1,1);
    lua_pushvalue(L,2);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc9);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc6);
  assert(lua_gettop(L) == 2);

  /* return self.values[key] */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: DenseVector:__tostring
 * function() */
int stuartml_linalg_DenseVector___tostring (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return '{' .. table.concat(self.values,',') .. '}' */
  lua_pushliteral(L,"{");
  lua_getfield(L,LUA_ENVIRONINDEX,"table");
  lua_pushliteral(L,"concat");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushliteral(L,",");
  lua_call(L,2,1);
  lua_pushliteral(L,"}");
  lua_concat(L,2);
  lua_concat(L,2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: DenseVector:_init
 * function(values) */
int stuartml_linalg_DenseVector__init (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local Vector = require 'stuart-ml.linalg.Vector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,SUPERMODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* Vector._init(self, values) */
  lua_pushliteral(L,"_init");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: DenseVector:argmax
 * function() */
int stuartml_linalg_DenseVector_argmax (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if self:size() == 0 then */
  enum { lc10 = 1 };
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushnumber(L,0);
  const int lc11 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc11);
  const int lc12 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc12) {

    /* return -1 */
    lua_pushnumber(L,-1);
    return 1;
    assert(lua_gettop(L) == 1);
  }
  else {

    /* else
     * local maxIdx = -1 */
    lua_pushnumber(L,-1);
    assert(lua_gettop(L) == 2);

    /* local maxValue = self.values[1] */
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    assert(lua_gettop(L) == 3);

    /* for i, value in ipairs(self.values) do
     * internal: local f, s, var = explist */
    enum { lc13 = 3 };
    lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_call(L,1,3);
    while (1) {

      /* internal: local var_1, ..., var_n = f(s, var)
       *           if var_1 == nil then break end
       *           var = var_1 */
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_pushvalue(L,-3);
      lua_call(L,2,2);
      if (lua_isnil(L,-2)) {
        break;
      }
      lua_pushvalue(L,-2);
      lua_replace(L,-4);

      /* internal: local i with idx 7
       * internal: local value with idx 8 */


      /* if value > maxValue then */
      enum { lc14 = 8 };
      const int lc15 = lua_lessthan(L,3,8);
      lua_pushboolean(L,lc15);
      const int lc16 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc16) {

        /* maxIdx = i */
        lua_pushvalue(L,7);
        lua_replace(L,2);
        assert(lua_gettop(L) == 8);

        /* maxValue = value */
        lua_pushvalue(L,8);
        lua_replace(L,3);
        assert(lua_gettop(L) == 8);
      }
      lua_settop(L,lc14);
      assert(lua_gettop(L) == 8);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,2);
    }
    lua_settop(L,lc13);
    assert(lua_gettop(L) == 3);

    /* return maxIdx */
    lua_pushvalue(L,2);
    return 1;
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc10);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuartml_linalg_DenseVector_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: DenseVector:copy
 * function() */
int stuartml_linalg_DenseVector_copy (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local DenseVector = require 'stuart-ml.linalg.DenseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return DenseVector.new(moses.clone(self.values)) */
  const int lc16 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,3);
  const int lc17 = lua_gettop(L);
  lua_pushliteral(L,"clone");
  lua_gettable(L,2);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc17),LUA_MULTRET);
  return (lua_gettop(L) - lc16);
  assert(lua_gettop(L) == 3);
}


/* name: DenseVector:foreachActive
 * function(f) */
int stuartml_linalg_DenseVector_foreachActive (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for i,value in ipairs(self.values) do
   * internal: local f, s, var = explist */
  enum { lc19 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local i with idx 6
     * internal: local value with idx 7 */


    /* f(i-1, value) */
    lua_pushvalue(L,2);
    lua_pushnumber(L,1);
    lc_sub(L,6,-1);
    lua_remove(L,-2);
    lua_pushvalue(L,7);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc19);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M.new
 * function(...) */
int stuartml_linalg_DenseVector_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


/* name: DenseVector:size
 * function() */
int stuartml_linalg_DenseVector_size (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return #self.values */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  const double lc20 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc20);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: DenseVector:toArray
 * function() */
int stuartml_linalg_DenseVector_toArray (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self.values */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: DenseVector:toDense
 * function() */
int stuartml_linalg_DenseVector_toDense (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(i,v) */
int stuartml_linalg_DenseVector_toSparse_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if v ~= 0 then */
  enum { lc22 = 2 };
  lua_pushnumber(L,0);
  const int lc23 = lua_equal(L,2,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc23);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc24 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc24) {

    /* ii[#ii+1] = i */
    lua_pushvalue(L,1);
    lc_getupvalue(L,lua_upvalueindex(1),1,1);
    lua_insert(L,-2);
    lc_getupvalue(L,lua_upvalueindex(1),1,1);
    const double lc25 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc25);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 2);

    /* vv[#vv+1] = v */
    lua_pushvalue(L,2);
    lc_getupvalue(L,lua_upvalueindex(1),0,2);
    lua_insert(L,-2);
    lc_getupvalue(L,lua_upvalueindex(1),0,2);
    const double lc26 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc26);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc22);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: DenseVector:toSparse
 * function() */
int stuartml_linalg_DenseVector_toSparse (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local ii = {} */
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc20 = 2 };
  assert((lua_gettop(L) == lc20));
  lua_newtable(L);
  lua_rawseti(L,lc20,1);
  assert(lua_gettop(L) == 2);

  /* local vv = {} */
  lc_newclosuretable(L,lc20);
  enum { lc21 = 3 };
  assert((lua_gettop(L) == lc21));
  lua_newtable(L);
  lua_rawseti(L,lc21,2);
  assert(lua_gettop(L) == 3);

  /* self:foreachActive(function(i,v)
   *     if v ~= 0 then
   *       ii[#ii+1] = i
   *       vv[#vv+1] = v
   *     end
   *   end) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"foreachActive");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,lc21);
  lua_pushcclosure(L,stuartml_linalg_DenseVector_toSparse_1,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);

  /* local SparseVector = require 'stuart-ml.linalg.SparseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return SparseVector.new(self:size(), ii, vv) */
  const int lc28 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lc_getupvalue(L,lc21,1,1);
  lc_getupvalue(L,lc21,0,2);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc28);
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgdvec_map[] = {
  { LSTRKEY("__eq")            , LFUNCVAL (stuartml_linalg_DenseVector___eq) },
  { LSTRKEY("__index")         , LFUNCVAL (stuartml_linalg_DenseVector___index) },
  { LSTRKEY("__tostring")      , LFUNCVAL (stuartml_linalg_DenseVector___tostring) },
  { LSTRKEY("_init")           , LFUNCVAL (stuartml_linalg_DenseVector__init) },
  { LSTRKEY("argmax")          , LFUNCVAL (stuartml_linalg_DenseVector_argmax) },
  { LSTRKEY("copy")            , LFUNCVAL (stuartml_linalg_DenseVector_copy) },
  { LSTRKEY("foreachActive")   , LFUNCVAL (stuartml_linalg_DenseVector_foreachActive) },
  { LSTRKEY("size")            , LFUNCVAL (stuartml_linalg_DenseVector_size) },
  { LSTRKEY("toArray")         , LFUNCVAL (stuartml_linalg_DenseVector_toArray) },
  { LSTRKEY("toDense")         , LFUNCVAL (stuartml_linalg_DenseVector_toDense) },
  { LSTRKEY("toSparse")        , LFUNCVAL (stuartml_linalg_DenseVector_toSparse) },

  // class framework
  { LSTRKEY("_base")           , LRO_ROVAL(stuartml_linalgvec_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuartml_linalgdvec_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuartml_linalg_DenseVector_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuartml_linalg_DenseVector_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("numActives")      , LFUNCVAL (stuartml_linalg_Vector_numActives) },
  { LSTRKEY("numNonzeros")     , LFUNCVAL (stuartml_linalg_Vector_numNonzeros) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgdvec( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGDVEC, stuartml_linalgdvec_map );
  return 1;
#endif
}
