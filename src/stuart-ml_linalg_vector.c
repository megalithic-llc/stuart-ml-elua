#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart-ml.linalg.Vector"


/* name: Vector:_init
 * function(values) */
int stuartml_linalg_Vector__init (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* self.values = values or {} */
  lua_pushvalue(L,2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_newtable(L);
  }
  lua_pushliteral(L,"values");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: Vector:numActives
 * function() */
int stuartml_linalg_Vector_numActives (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return #self.values */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  const double lc1 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: M.classof
 * function(obj) */
int stuartml_linalg_Vector_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: M.new
 * function(...) */
int stuartml_linalg_Vector_new (lua_State * L) {
  return stuart_class_shared_new(L, MODULE);
}


/* function(r,v) */
int stuartml_linalg_Vector_numNonzeros_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if v ~= 0 then */
  enum { lc2 = 2 };
  lua_pushnumber(L,0);
  const int lc3 = lua_equal(L,2,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc3);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc4 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc4) {

    /* r = r + 1 */
    lua_pushnumber(L,1);
    lc_add(L,1,-1);
    lua_remove(L,-2);
    lua_replace(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc2);
  assert(lua_gettop(L) == 2);

  /* return r */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: Vector:numNonzeros
 * function() */
int stuartml_linalg_Vector_numNonzeros (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local nnz = moses.reduce(self.values, function(r,v)
   *     if v ~= 0 then r = r + 1 end
   *     return r
   *   end, 0) */
  lua_pushliteral(L,"reduce");
  lua_gettable(L,2);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushcfunction(L,stuartml_linalg_Vector_numNonzeros_1);
  lua_pushnumber(L,0);
  lua_call(L,3,1);
  assert(lua_gettop(L) == 3);

  /* return nnz */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 3);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgvec_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL (stuartml_linalg_Vector__init) },
  { LSTRKEY("numActives")      , LFUNCVAL (stuartml_linalg_Vector_numActives) },
  { LSTRKEY("numNonzeros")     , LFUNCVAL (stuartml_linalg_Vector_numNonzeros) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuartml_linalgvec_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuartml_linalgvec_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuartml_linalg_Vector_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuartml_linalg_Vector_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgvec( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGVEC, stuartml_linalgvec_map );
  return 1;
#endif
}
