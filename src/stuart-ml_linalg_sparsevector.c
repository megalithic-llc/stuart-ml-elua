#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart-ml_linalg_vector.h"

#define MODULE "stuart-ml.linalg.SparseVector"
#define SUPERMODULE "stuart-ml.linalg.Vector"


/* name: SparseVector.__eq
 * function(a, b) */
int stuartml_linalg_SparseVector___eq (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if a:size() ~= b:size() then */
  enum { lc11 = 2 };
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  const int lc12 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc12);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc13 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc13) {

    /* return false */
    lua_pushboolean(L,0);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc11);
  assert(lua_gettop(L) == 2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* if class.istype(b,SparseVector) then */
  enum { lc14 = 3 };
  lc_getupvalue(L,lua_upvalueindex(1),1,1);
  lua_pushliteral(L,"istype");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,2);
  lc_getupvalue(L,lua_upvalueindex(1),0,2);
  lua_call(L,2,1);
  const int lc15 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc15) {

    /* if not moses.same(a.indices, b.indices) then */
    enum { lc16 = 3 };
    lua_pushliteral(L,"same");
    lua_gettable(L,3);
    lua_pushliteral(L,"indices");
    lua_gettable(L,1);
    lua_pushliteral(L,"indices");
    lua_gettable(L,2);
    lua_call(L,2,1);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
    const int lc17 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc17) {

      /* return false */
      lua_pushboolean(L,0);
      return 1;
      assert(lua_gettop(L) == 3);
    }
    lua_settop(L,lc16);
    assert(lua_gettop(L) == 3);

    /* return moses.same(a.values, b.values) */
    const int lc18 = lua_gettop(L);
    lua_pushliteral(L,"same");
    lua_gettable(L,3);
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushliteral(L,"values");
    lua_gettable(L,2);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc18);
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc14);
  assert(lua_gettop(L) == 3);

  /* -- This next section only runs in Lua 5.3+, and supports the equality test
   * -- of a SparseVector against a DenseVector
   * local DenseVector = require 'stuart-ml.linalg.DenseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* if class.istype(b,DenseVector) then */
  enum { lc19 = 4 };
  lc_getupvalue(L,lua_upvalueindex(1),1,1);
  lua_pushliteral(L,"istype");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,2);
  lua_pushvalue(L,4);
  lua_call(L,2,1);
  const int lc20 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc20) {

    /* if not moses.same(a.values, b.values) then */
    enum { lc21 = 4 };
    lua_pushliteral(L,"same");
    lua_gettable(L,3);
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushliteral(L,"values");
    lua_gettable(L,2);
    lua_call(L,2,1);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
    const int lc22 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc22) {

      /* return false */
      lua_pushboolean(L,0);
      return 1;
      assert(lua_gettop(L) == 4);
    }
    lua_settop(L,lc21);
    assert(lua_gettop(L) == 4);

    /* local bIndices = moses.range(1, a:size()) */
    lua_pushliteral(L,"range");
    lua_gettable(L,3);
    const int lc23 = lua_gettop(L);
    lua_pushnumber(L,1);
    lua_pushvalue(L,1);
    lua_pushliteral(L,"size");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,LUA_MULTRET);
    lua_call(L,(lua_gettop(L) - lc23),1);
    assert(lua_gettop(L) == 5);

    /* return moses.same(a.indices, bIndices) */
    const int lc24 = lua_gettop(L);
    lua_pushliteral(L,"same");
    lua_gettable(L,3);
    lua_pushliteral(L,"indices");
    lua_gettable(L,1);
    lua_pushvalue(L,5);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc24);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc19);
  assert(lua_gettop(L) == 4);

  /* return false */
  lua_pushboolean(L,0);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: SparseVector:__index
 * function(key) */
int stuartml_linalg_SparseVector___index (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if type(key)~='number' then */
  enum { lc25 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"type");
  lua_pushvalue(L,2);
  lua_call(L,1,1);
  lua_pushliteral(L,"number");
  const int lc26 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc26);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc27 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc27) {

    /* return rawget(getmetatable(self), key) */
    const int lc28 = lua_gettop(L);
    lua_getfield(L,LUA_ENVIRONINDEX,"rawget");
    lua_getfield(L,LUA_ENVIRONINDEX,"getmetatable");
    lua_pushvalue(L,1);
    lua_call(L,1,1);
    lua_pushvalue(L,2);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc28);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc25);
  assert(lua_gettop(L) == 2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local i = moses.indexOf(self.indices, key) */
  lua_pushliteral(L,"indexOf");
  lua_gettable(L,3);
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* if i == nil then */
  enum { lc29 = 4 };
  lua_pushnil(L);
  const int lc30 = lua_equal(L,4,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc30);
  const int lc31 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc31) {

    /* return 0 */
    lua_pushnumber(L,0);
    return 1;
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc29);
  assert(lua_gettop(L) == 4);

  /* return self.values[i] */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushvalue(L,4);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: SparseVector:__tostring
 * function() */
int stuartml_linalg_SparseVector___tostring (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return '(' .. self._size .. ',{'
   *     .. table.concat(self.indices,',') .. '},{'
   *     .. table.concat(self.values,',') .. '})' */
  lua_pushliteral(L,"(");
  lua_pushliteral(L,"_size");
  lua_gettable(L,1);
  lua_pushliteral(L,",{");
  lua_getfield(L,LUA_ENVIRONINDEX,"table");
  lua_pushliteral(L,"concat");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  lua_pushliteral(L,",");
  lua_call(L,2,1);
  lua_pushliteral(L,"},{");
  lua_getfield(L,LUA_ENVIRONINDEX,"table");
  lua_pushliteral(L,"concat");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushliteral(L,",");
  lua_call(L,2,1);
  lua_pushliteral(L,"})");
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: SparseVector:_init
 * function(size, indices, values) */
int stuartml_linalg_SparseVector__init (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);

  /* local Vector = require 'stuart-ml.linalg.Vector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,SUPERMODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* assert(#indices == #values, 'Sparse vectors require that the dimension of the '
   *     .. 'indices match the dimension of the values. You provided ' .. #indices .. ' indices and '
   *     .. #values .. ' values') */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const double lc3 = lua_objlen(L,3);
  lua_pushnumber(L,lc3);
  const double lc4 = lua_objlen(L,4);
  lua_pushnumber(L,lc4);
  const int lc5 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc5);
  lua_pushliteral(L,"Sparse vectors require that the dimension of the ");
  lua_pushliteral(L,"indices match the dimension of the values. You provided ");
  const double lc6 = lua_objlen(L,3);
  lua_pushnumber(L,lc6);
  lua_pushliteral(L," indices and ");
  const double lc7 = lua_objlen(L,4);
  lua_pushnumber(L,lc7);
  lua_pushliteral(L," values");
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 5);

  /* assert(#indices <= size, 'You provided ' .. #indices .. ' indices and values, '
   *     .. 'which exceeds the specified vector size ' .. size) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const double lc8 = lua_objlen(L,3);
  lua_pushnumber(L,lc8);
  const int lc9 = lc_le(L,-1,2);
  lua_pop(L,1);
  lua_pushboolean(L,lc9);
  lua_pushliteral(L,"You provided ");
  const double lc10 = lua_objlen(L,3);
  lua_pushnumber(L,lc10);
  lua_pushliteral(L," indices and values, ");
  lua_pushliteral(L,"which exceeds the specified vector size ");
  lua_pushvalue(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 5);

  /* self._size = size */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"_size");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 5);

  /* self.indices = indices */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"indices");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 5);

  /* Vector._init(self, values) */
  lua_pushliteral(L,"_init");
  lua_gettable(L,5);
  lua_pushvalue(L,1);
  lua_pushvalue(L,4);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 5);
  return 0;
}


/* name: SparseVector:argmax
 * function() */
int stuartml_linalg_SparseVector_argmax (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if self._size == 0 then */
  enum { lc32 = 1 };
  lua_pushliteral(L,"_size");
  lua_gettable(L,1);
  lua_pushnumber(L,0);
  const int lc33 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc33);
  const int lc34 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc34) {

    /* return -1 */
    lua_pushnumber(L,-1);
    return 1;
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc32);
  assert(lua_gettop(L) == 1);

  /* if self:numActives() == 0 then */
  enum { lc35 = 1 };
  lua_pushvalue(L,1);
  lua_pushliteral(L,"numActives");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushnumber(L,0);
  const int lc36 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc36);
  const int lc37 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc37) {

    /* return 0 */
    lua_pushnumber(L,0);
    return 1;
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc35);
  assert(lua_gettop(L) == 1);

  /* -- Find the max active entry
   * local maxIdx = self.indices[1] */
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  lua_pushnumber(L,1);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 2);

  /* local maxValue = self.values[1] */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushnumber(L,1);
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 3);

  /* local maxJ = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 4);

  /* local na = self:numActives() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"numActives");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* for j=2,na do */
  lua_pushnumber(L,2);
  if (!((lua_isnumber(L,-1) && lua_isnumber(L,5)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc38_var = lua_tonumber(L,-1);
  const double lc39_limit = lua_tonumber(L,5);
  const double lc40_step = 1;
  lua_pop(L,1);
  enum { lc41 = 5 };
  while ((((lc40_step > 0) && (lc38_var <= lc39_limit)) || ((lc40_step <= 0) && (lc38_var >= lc39_limit)))) {

    /* internal: local j at index 6 */
    lua_pushnumber(L,lc38_var);

    /* local v = self.values[j] */
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushvalue(L,6);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    assert(lua_gettop(L) == 7);

    /* if v > maxValue then */
    enum { lc42 = 7 };
    const int lc43 = lua_lessthan(L,3,7);
    lua_pushboolean(L,lc43);
    const int lc44 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc44) {

      /* maxValue = v */
      lua_pushvalue(L,7);
      lua_replace(L,3);
      assert(lua_gettop(L) == 7);

      /* maxIdx = self.indices[j] */
      lua_pushliteral(L,"indices");
      lua_gettable(L,1);
      lua_pushvalue(L,6);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_replace(L,2);
      assert(lua_gettop(L) == 7);

      /* maxJ = j */
      lua_pushvalue(L,6);
      lua_replace(L,4);
      assert(lua_gettop(L) == 7);
    }
    lua_settop(L,lc42);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
    lc38_var += lc40_step;
  }
  lua_settop(L,lc41);
  assert(lua_gettop(L) == 5);

  /* -- If the max active entry is nonpositive and there exists inactive ones, find the first zero.
   * if maxValue <= 0.0 and na < self._size then */
  enum { lc45 = 5 };
  lua_pushnumber(L,0);
  const int lc46 = lc_le(L,3,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc46);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"_size");
    lua_gettable(L,1);
    const int lc47 = lua_lessthan(L,5,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc47);
  }
  const int lc48 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc48) {

    /* if maxValue == 0.0 then */
    enum { lc49 = 5 };
    lua_pushnumber(L,0);
    const int lc50 = lua_equal(L,3,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc50);
    const int lc51 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc51) {

      /* -- If there exists an inactive entry before maxIdx, find it and return its index.
       * if maxJ < maxIdx then */
      enum { lc52 = 5 };
      const int lc53 = lua_lessthan(L,4,2);
      lua_pushboolean(L,lc53);
      const int lc54 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc54) {

        /* local k = 0 */
        lua_pushnumber(L,0);
        assert(lua_gettop(L) == 6);

        /* while k < maxJ and self.indices[k+1] == k do */
        enum { lc55 = 6 };
        while (1) {
          const int lc56 = lua_lessthan(L,6,4);
          lua_pushboolean(L,lc56);
          if (lua_toboolean(L,-1)) {
            lua_pop(L,1);
            lua_pushliteral(L,"indices");
            lua_gettable(L,1);
            lua_pushnumber(L,1);
            lc_add(L,6,-1);
            lua_remove(L,-2);
            lua_gettable(L,-2);
            lua_remove(L,-2);
            lua_pushvalue(L,6);
            const int lc57 = lua_equal(L,-2,-1);
            lua_pop(L,2);
            lua_pushboolean(L,lc57);
          }
          if (!(lua_toboolean(L,-1))) {
            break;
          }
          lua_pop(L,1);

          /* k = k + 1 */
          lua_pushnumber(L,1);
          lc_add(L,6,-1);
          lua_remove(L,-2);
          lua_replace(L,6);
          assert(lua_gettop(L) == 6);
        }
        lua_settop(L,lc55);
        assert(lua_gettop(L) == 6);

        /* maxIdx = k */
        lua_pushvalue(L,6);
        lua_replace(L,2);
        assert(lua_gettop(L) == 6);
      }
      lua_settop(L,lc52);
      assert(lua_gettop(L) == 5);
    }
    else {

      /* else
       * local k = 0 */
      lua_pushnumber(L,0);
      assert(lua_gettop(L) == 6);

      /* while k < na and self.indices[k+1] == k do */
      enum { lc58 = 6 };
      while (1) {
        const int lc59 = lua_lessthan(L,6,5);
        lua_pushboolean(L,lc59);
        if (lua_toboolean(L,-1)) {
          lua_pop(L,1);
          lua_pushliteral(L,"indices");
          lua_gettable(L,1);
          lua_pushnumber(L,1);
          lc_add(L,6,-1);
          lua_remove(L,-2);
          lua_gettable(L,-2);
          lua_remove(L,-2);
          lua_pushvalue(L,6);
          const int lc60 = lua_equal(L,-2,-1);
          lua_pop(L,2);
          lua_pushboolean(L,lc60);
        }
        if (!(lua_toboolean(L,-1))) {
          break;
        }
        lua_pop(L,1);

        /* k = k + 1 */
        lua_pushnumber(L,1);
        lc_add(L,6,-1);
        lua_remove(L,-2);
        lua_replace(L,6);
        assert(lua_gettop(L) == 6);
      }
      lua_settop(L,lc58);
      assert(lua_gettop(L) == 6);

      /* maxIdx = k */
      lua_pushvalue(L,6);
      lua_replace(L,2);
      assert(lua_gettop(L) == 6);
    }
    lua_settop(L,lc49);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc45);
  assert(lua_gettop(L) == 5);

  /* return maxIdx */
  lua_pushvalue(L,2);
  return 1;
  assert(lua_gettop(L) == 5);
}


/* name: M.classof
 * function(obj) */
int stuartml_linalg_SparseVector_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: SparseVector:copy
 * function() */
int stuartml_linalg_SparseVector_copy (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local SparseVector = require 'stuart-ml.linalg.SparseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return SparseVector.new(self._size, moses.clone(self.indices), moses.clone(self.values)) */
  const int lc61 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,3);
  const int lc62 = lua_gettop(L);
  lua_pushliteral(L,"_size");
  lua_gettable(L,1);
  lua_pushliteral(L,"clone");
  lua_gettable(L,2);
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  lua_call(L,1,1);
  lua_pushliteral(L,"clone");
  lua_gettable(L,2);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc62),LUA_MULTRET);
  return (lua_gettop(L) - lc61);
  assert(lua_gettop(L) == 3);
}


/* name: SparseVector:foreachActive
 * function(f) */
int stuartml_linalg_SparseVector_foreachActive (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for i,value in ipairs(self.values) do
   * internal: local f, s, var = explist */
  enum { lc63 = 2 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local i with idx 6
     * internal: local value with idx 7 */


    /* f(self.indices[i], value) */
    lua_pushvalue(L,2);
    lua_pushliteral(L,"indices");
    lua_gettable(L,1);
    lua_pushvalue(L,6);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,7);
    lua_call(L,2,0);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc63);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M.new
 * function(...) */
int stuartml_linalg_SparseVector_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


/* name: SparseVector:size
 * function() */
int stuartml_linalg_SparseVector_size (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return self._size */
  lua_pushliteral(L,"_size");
  lua_gettable(L,1);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: SparseVector:toArray
 * function() */
int stuartml_linalg_SparseVector_toArray (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local data = moses.rep(0, self._size) */
  lua_pushliteral(L,"rep");
  lua_gettable(L,2);
  lua_pushnumber(L,0);
  lua_pushliteral(L,"_size");
  lua_gettable(L,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* for i,k in ipairs(self.indices) do
   * internal: local f, s, var = explist */
  enum { lc64 = 3 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local i with idx 7
     * internal: local k with idx 8 */


    /* data[k+1] = self.values[i] */
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushvalue(L,7);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushnumber(L,1);
    lc_add(L,8,-1);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,3);
    assert(lua_gettop(L) == 8);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc64);
  assert(lua_gettop(L) == 3);

  /* return data */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: SparseVector:toDense
 * function() */
int stuartml_linalg_SparseVector_toDense (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local DenseVector = require 'stuart-ml.linalg.DenseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return DenseVector.new(self:toArray()) */
  const int lc65 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,2);
  const int lc66 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toArray");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc66),LUA_MULTRET);
  return (lua_gettop(L) - lc65);
  assert(lua_gettop(L) == 2);
}


/* function(i,v) */
int stuartml_linalg_SparseVector_toSparse_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if v ~= 0 then */
  enum { lc72 = 2 };
  lua_pushnumber(L,0);
  const int lc73 = lua_equal(L,2,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc73);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc74 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc74) {

    /* ii[#ii+1] = i */
    lua_pushvalue(L,1);
    lc_getupvalue(L,lua_upvalueindex(1),1,3);
    lua_insert(L,-2);
    lc_getupvalue(L,lua_upvalueindex(1),1,3);
    const double lc75 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc75);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 2);

    /* vv[#vv+1] = v */
    lua_pushvalue(L,2);
    lc_getupvalue(L,lua_upvalueindex(1),0,4);
    lua_insert(L,-2);
    lc_getupvalue(L,lua_upvalueindex(1),0,4);
    const double lc76 = lua_objlen(L,-1);
    lua_pop(L,1);
    lua_pushnumber(L,lc76);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc72);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: SparseVector:toSparse
 * function() */
int stuartml_linalg_SparseVector_toSparse (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* if self:numActives() == self:numNonzeros() then */
  enum { lc67 = 1 };
  lua_pushvalue(L,1);
  lua_pushliteral(L,"numActives");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"numNonzeros");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  const int lc68 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc68);
  const int lc69 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc69) {

    /* return self */
    lua_pushvalue(L,1);
    return 1;
    assert(lua_gettop(L) == 1);
  }
  lua_settop(L,lc67);
  assert(lua_gettop(L) == 1);

  /* local ii = {} */
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc70 = 2 };
  assert((lua_gettop(L) == lc70));
  lua_newtable(L);
  lua_rawseti(L,lc70,3);
  assert(lua_gettop(L) == 2);

  /* local vv = {} */
  lc_newclosuretable(L,lc70);
  enum { lc71 = 3 };
  assert((lua_gettop(L) == lc71));
  lua_newtable(L);
  lua_rawseti(L,lc71,4);
  assert(lua_gettop(L) == 3);

  /* self:foreachActive(function(i,v)
   *     if v ~= 0 then
   *       ii[#ii+1] = i
   *       vv[#vv+1] = v
   *     end
   *   end) */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"foreachActive");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,lc71);
  lua_pushcclosure(L,stuartml_linalg_SparseVector_toSparse_1,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 3);

  /* local SparseVector = require 'stuart-ml.linalg.SparseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return SparseVector.new(self:size(), ii, vv) */
  const int lc78 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lc_getupvalue(L,lc71,1,3);
  lc_getupvalue(L,lc71,0,4);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc78);
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgsvec_map[] = {
  { LSTRKEY("__eq")            , LFUNCVAL (stuartml_linalg_SparseVector___eq) },
  { LSTRKEY("__index")         , LFUNCVAL (stuartml_linalg_SparseVector___index) },
  { LSTRKEY("__tostring")      , LFUNCVAL (stuartml_linalg_SparseVector___tostring) },
  { LSTRKEY("_init")           , LFUNCVAL (stuartml_linalg_SparseVector__init) },
  { LSTRKEY("argmax")          , LFUNCVAL (stuartml_linalg_SparseVector_argmax) },
  { LSTRKEY("copy")            , LFUNCVAL (stuartml_linalg_SparseVector_copy) },
  { LSTRKEY("foreachActive")   , LFUNCVAL (stuartml_linalg_SparseVector_foreachActive) },
  { LSTRKEY("size")            , LFUNCVAL (stuartml_linalg_SparseVector_size) },
  { LSTRKEY("toArray")         , LFUNCVAL (stuartml_linalg_SparseVector_toArray) },
  { LSTRKEY("toDense")         , LFUNCVAL (stuartml_linalg_SparseVector_toDense) },
  { LSTRKEY("toSparse")         , LFUNCVAL (stuartml_linalg_SparseVector_toSparse) },

  // class framework
  { LSTRKEY("_base")           , LRO_ROVAL(stuartml_linalgvec_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuartml_linalgsvec_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuartml_linalg_SparseVector_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuartml_linalg_SparseVector_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("numActives")      , LFUNCVAL (stuartml_linalg_Vector_numActives) },
  { LSTRKEY("numNonzeros")     , LFUNCVAL (stuartml_linalg_Vector_numNonzeros) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgsvec( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGSVEC, stuartml_linalgsvec_map );
  return 1;
#endif
}
