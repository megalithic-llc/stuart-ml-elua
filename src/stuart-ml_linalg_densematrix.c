#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart-ml_linalg_matrix.h"

#define MODULE "stuart-ml.linalg.DenseMatrix"
#define SUPERMODULE "stuart-ml.linalg.Matrix"


/* name: DenseMatrix:__eq
 * function(other) */
int stuartml_linalg_DenseMatrix___eq (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if not class.istype(other, Matrix) then */
  enum { lc6 = 2 };
  lc_getupvalue(L,lua_upvalueindex(1),2,1);
  lua_pushliteral(L,"istype");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,2);
  lc_getupvalue(L,lua_upvalueindex(1),1,2);
  lua_call(L,2,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc7 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc7) {

    /* return false */
    lua_pushboolean(L,0);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc6);
  assert(lua_gettop(L) == 2);

  /* if self.numRows ~= other.numRows or self.numCols ~= other.numCols then */
  enum { lc8 = 2 };
  lua_pushliteral(L,"numRows");
  lua_gettable(L,1);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,2);
  const int lc9 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc9);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushliteral(L,"numCols");
    lua_gettable(L,1);
    lua_pushliteral(L,"numCols");
    lua_gettable(L,2);
    const int lc10 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc10);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
  }
  const int lc11 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc11) {

    /* return false */
    lua_pushboolean(L,0);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc8);
  assert(lua_gettop(L) == 2);

  /* for row = 0, self.numRows-1 do */
  lua_pushnumber(L,0);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,1);
  lua_pushnumber(L,1);
  lc_sub(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc12_var = lua_tonumber(L,-2);
  const double lc13_limit = lua_tonumber(L,-1);
  const double lc14_step = 1;
  lua_pop(L,2);
  enum { lc15 = 2 };
  while ((((lc14_step > 0) && (lc12_var <= lc13_limit)) || ((lc14_step <= 0) && (lc12_var >= lc13_limit)))) {

    /* internal: local row at index 3 */
    lua_pushnumber(L,lc12_var);

    /* for col = 0, self.numCols-1 do */
    lua_pushnumber(L,0);
    lua_pushliteral(L,"numCols");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_sub(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc16_var = lua_tonumber(L,-2);
    const double lc17_limit = lua_tonumber(L,-1);
    const double lc18_step = 1;
    lua_pop(L,2);
    enum { lc19 = 3 };
    while ((((lc18_step > 0) && (lc16_var <= lc17_limit)) || ((lc18_step <= 0) && (lc16_var >= lc17_limit)))) {

      /* internal: local col at index 4 */
      lua_pushnumber(L,lc16_var);

      /* if self.values[self:index(row,col)] ~= other.values[other:index(row,col)] then */
      enum { lc20 = 4 };
      lua_pushliteral(L,"values");
      lua_gettable(L,1);
      lua_pushvalue(L,1);
      lua_pushliteral(L,"index");
      lua_gettable(L,-2);
      lua_insert(L,-2);
      lua_pushvalue(L,3);
      lua_pushvalue(L,4);
      lua_call(L,3,1);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"values");
      lua_gettable(L,2);
      lua_pushvalue(L,2);
      lua_pushliteral(L,"index");
      lua_gettable(L,-2);
      lua_insert(L,-2);
      lua_pushvalue(L,3);
      lua_pushvalue(L,4);
      lua_call(L,3,1);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      const int lc21 = lua_equal(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc21);
      lua_pushboolean(L,!(lua_toboolean(L,-1)));
      lua_remove(L,-2);
      const int lc22 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc22) {

        /* return false */
        lua_pushboolean(L,0);
        return 1;
        assert(lua_gettop(L) == 4);
      }
      lua_settop(L,lc20);
      assert(lua_gettop(L) == 4);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,1);
      lc16_var += lc18_step;
    }
    lua_settop(L,lc19);
    assert(lua_gettop(L) == 3);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc12_var += lc14_step;
  }
  lua_settop(L,lc15);
  assert(lua_gettop(L) == 2);

  /* return true */
  lua_pushboolean(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: DenseMatrix:_init
 * function(numRows, numCols, values, isTransposed) */
int stuartml_linalg_DenseMatrix__init (lua_State * L) {
  enum { lc_nformalargs = 5 };
  lua_settop(L,5);

  /* assert(#values == numRows * numCols) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const double lc4 = lua_objlen(L,4);
  lua_pushnumber(L,lc4);
  lc_mul(L,2,3);
  const int lc5 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc5);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 5);

  /* local Matrix = require 'stuart-ml.linalg.Matrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.Matrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* Matrix:_init(self) */
  lua_pushvalue(L,6);
  lua_pushliteral(L,"_init");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 6);

  /* self.numRows = numRows */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"numRows");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 6);

  /* self.numCols = numCols */
  lua_pushvalue(L,3);
  lua_pushliteral(L,"numCols");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 6);

  /* self.values = values */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"values");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 6);

  /* self.isTransposed = isTransposed or false */
  lua_pushvalue(L,5);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushboolean(L,0);
  }
  lua_pushliteral(L,"isTransposed");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 6);
  return 0;
}


/* name: DenseMatrix:asBreeze
 * function() */
int stuartml_linalg_DenseMatrix_asBreeze (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: DenseMatrix:copy
 * function() */
int stuartml_linalg_DenseMatrix_copy (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: M.classof
 * function(obj) */
int stuartml_linalg_DenseMatrix_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: DenseMatrix.eye
 * function(n) */
int stuartml_linalg_DenseMatrix_eye (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local identity = DenseMatrix.zeros(n, n) */
  lua_pushliteral(L,"zeros");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_pushvalue(L,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* for i=0, n-1 do */
  lua_pushnumber(L,0);
  lua_pushnumber(L,1);
  lc_sub(L,1,-1);
  lua_remove(L,-2);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc23_var = lua_tonumber(L,-2);
  const double lc24_limit = lua_tonumber(L,-1);
  const double lc25_step = 1;
  lua_pop(L,2);
  enum { lc26 = 3 };
  while ((((lc25_step > 0) && (lc23_var <= lc24_limit)) || ((lc25_step <= 0) && (lc23_var >= lc24_limit)))) {

    /* internal: local i at index 4 */
    lua_pushnumber(L,lc23_var);

    /* identity:update(i, i, 1.0) */
    lua_pushvalue(L,3);
    lua_pushliteral(L,"update");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,4);
    lua_pushvalue(L,4);
    lua_pushnumber(L,1);
    lua_call(L,4,0);
    assert(lua_gettop(L) == 4);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc23_var += lc25_step;
  }
  lua_settop(L,lc26);
  assert(lua_gettop(L) == 3);

  /* return identity */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: DenseMatrix:foreachActive
 * function(f) */
int stuartml_linalg_DenseMatrix_foreachActive (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if not self.isTransposed then */
  enum { lc23 = 2 };
  lua_pushliteral(L,"isTransposed");
  lua_gettable(L,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc24 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc24) {

    /* -- outer loop over columns
     * for j = 0, self.numCols-1 do */
    lua_pushnumber(L,0);
    lua_pushliteral(L,"numCols");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_sub(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc25_var = lua_tonumber(L,-2);
    const double lc26_limit = lua_tonumber(L,-1);
    const double lc27_step = 1;
    lua_pop(L,2);
    enum { lc28 = 2 };
    while ((((lc27_step > 0) && (lc25_var <= lc26_limit)) || ((lc27_step <= 0) && (lc25_var >= lc26_limit)))) {

      /* internal: local j at index 3 */
      lua_pushnumber(L,lc25_var);

      /* local indStart = j * self.numRows */
      lua_pushliteral(L,"numRows");
      lua_gettable(L,1);
      lc_mul(L,3,-1);
      lua_remove(L,-2);
      assert(lua_gettop(L) == 4);

      /* for i = 0, self.numRows-1 do */
      lua_pushnumber(L,0);
      lua_pushliteral(L,"numRows");
      lua_gettable(L,1);
      lua_pushnumber(L,1);
      lc_sub(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
        luaL_error(L,"'for' limit must be a number");
      }
      double lc29_var = lua_tonumber(L,-2);
      const double lc30_limit = lua_tonumber(L,-1);
      const double lc31_step = 1;
      lua_pop(L,2);
      enum { lc32 = 4 };
      while ((((lc31_step > 0) && (lc29_var <= lc30_limit)) || ((lc31_step <= 0) && (lc29_var >= lc30_limit)))) {

        /* internal: local i at index 5 */
        lua_pushnumber(L,lc29_var);

        /* f(i, j, self.values[1 + indStart + i]) */
        lua_pushvalue(L,2);
        lua_pushvalue(L,5);
        lua_pushvalue(L,3);
        lua_pushliteral(L,"values");
        lua_gettable(L,1);
        lua_pushnumber(L,1);
        lc_add(L,-1,4);
        lua_remove(L,-2);
        lc_add(L,-1,5);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        lua_call(L,3,0);
        assert(lua_gettop(L) == 5);

        /* internal: stack cleanup on scope exit */
        lua_pop(L,1);
        lc29_var += lc31_step;
      }
      lua_settop(L,lc32);
      assert(lua_gettop(L) == 4);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,2);
      lc25_var += lc27_step;
    }
    lua_settop(L,lc28);
    assert(lua_gettop(L) == 2);
  }
  else {

    /* else
     * -- outer loop over rows
     * for i = 0, self.numRows-1 do */
    lua_pushnumber(L,0);
    lua_pushliteral(L,"numRows");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_sub(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc33_var = lua_tonumber(L,-2);
    const double lc34_limit = lua_tonumber(L,-1);
    const double lc35_step = 1;
    lua_pop(L,2);
    enum { lc36 = 2 };
    while ((((lc35_step > 0) && (lc33_var <= lc34_limit)) || ((lc35_step <= 0) && (lc33_var >= lc34_limit)))) {

      /* internal: local i at index 3 */
      lua_pushnumber(L,lc33_var);

      /* local indStart = i * self.numCols */
      lua_pushliteral(L,"numCols");
      lua_gettable(L,1);
      lc_mul(L,3,-1);
      lua_remove(L,-2);
      assert(lua_gettop(L) == 4);

      /* for j = 0, self.numCols-1 do */
      lua_pushnumber(L,0);
      lua_pushliteral(L,"numCols");
      lua_gettable(L,1);
      lua_pushnumber(L,1);
      lc_sub(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
        luaL_error(L,"'for' limit must be a number");
      }
      double lc37_var = lua_tonumber(L,-2);
      const double lc38_limit = lua_tonumber(L,-1);
      const double lc39_step = 1;
      lua_pop(L,2);
      enum { lc40 = 4 };
      while ((((lc39_step > 0) && (lc37_var <= lc38_limit)) || ((lc39_step <= 0) && (lc37_var >= lc38_limit)))) {

        /* internal: local j at index 5 */
        lua_pushnumber(L,lc37_var);

        /* f(i, j, self.values[1 + indStart + j]) */
        lua_pushvalue(L,2);
        lua_pushvalue(L,3);
        lua_pushvalue(L,5);
        lua_pushliteral(L,"values");
        lua_gettable(L,1);
        lua_pushnumber(L,1);
        lc_add(L,-1,4);
        lua_remove(L,-2);
        lc_add(L,-1,5);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        lua_call(L,3,0);
        assert(lua_gettop(L) == 5);

        /* internal: stack cleanup on scope exit */
        lua_pop(L,1);
        lc37_var += lc39_step;
      }
      lua_settop(L,lc40);
      assert(lua_gettop(L) == 4);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,2);
      lc33_var += lc35_step;
    }
    lua_settop(L,lc36);
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc23);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: DenseMatrix:get
 * function(i, j) */
int stuartml_linalg_DenseMatrix_get (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* if j == nil then */
  enum { lc41 = 3 };
  lua_pushnil(L);
  const int lc42 = lua_equal(L,3,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc42);
  const int lc43 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc43) {

    /* return self.values[i+1] */
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_add(L,2,-1);
    lua_remove(L,-2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    return 1;
    assert(lua_gettop(L) == 3);
  }
  else {

    /* else
     * return self.values[self:index(i, j)] */
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushvalue(L,1);
    lua_pushliteral(L,"index");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,2);
    lua_pushvalue(L,3);
    lua_call(L,3,1);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    return 1;
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc41);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: DenseMatrix:index
 * function(i, j) */
int stuartml_linalg_DenseMatrix_index (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* assert(i >= 0 and i < self.numRows) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnumber(L,0);
  const int lc44 = lc_le(L,-1,2);
  lua_pop(L,1);
  lua_pushboolean(L,lc44);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"numRows");
    lua_gettable(L,1);
    const int lc45 = lua_lessthan(L,2,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc45);
  }
  lua_call(L,1,0);
  assert(lua_gettop(L) == 3);

  /* assert(j >= 0 and j < self.numCols) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnumber(L,0);
  const int lc46 = lc_le(L,-1,3);
  lua_pop(L,1);
  lua_pushboolean(L,lc46);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"numCols");
    lua_gettable(L,1);
    const int lc47 = lua_lessthan(L,3,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc47);
  }
  lua_call(L,1,0);
  assert(lua_gettop(L) == 3);

  /* if not self.isTransposed then */
  enum { lc48 = 3 };
  lua_pushliteral(L,"isTransposed");
  lua_gettable(L,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  const int lc49 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc49) {

    /* return 1 + i + self.numRows * j */
    lua_pushnumber(L,1);
    lc_add(L,-1,2);
    lua_remove(L,-2);
    lua_pushliteral(L,"numRows");
    lua_gettable(L,1);
    lc_mul(L,-1,3);
    lua_remove(L,-2);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    return 1;
    assert(lua_gettop(L) == 3);
  }
  else {

    /* else
     * return 1 + j + self.numCols * i */
    lua_pushnumber(L,1);
    lc_add(L,-1,3);
    lua_remove(L,-2);
    lua_pushliteral(L,"numCols");
    lua_gettable(L,1);
    lc_mul(L,-1,2);
    lua_remove(L,-2);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    return 1;
    assert(lua_gettop(L) == 3);
  }
  lua_settop(L,lc48);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: DenseMatrix:map
 * function() */
int stuartml_linalg_DenseMatrix_map (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return DenseMatrix.new(self.numRows, self.numCols, moses.map(self.values, f), self.isTransposed) */
  const int lc54 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,1);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,1);
  lua_pushliteral(L,"map");
  lua_gettable(L,3);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  lua_pushliteral(L,"isTransposed");
  lua_gettable(L,1);
  lua_call(L,4,LUA_MULTRET);
  return (lua_gettop(L) - lc54);
  assert(lua_gettop(L) == 4);
}


/* name: M.new
 * function(...) */
int stuartml_linalg_DenseMatrix_new (lua_State * L) {
  return stuart_class_shared_new_with_super(L, MODULE, SUPERMODULE);
}


/* name: DenseMatrix:numActives
 * function() */
int stuartml_linalg_DenseMatrix_numActives (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return #self.values */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  const double lc50 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc50);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* function(x) */
int stuartml_linalg_DenseMatrix_numNonzeros_1 (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return x ~= 0 */
  lua_pushnumber(L,0);
  const int lc52 = lua_equal(L,1,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc52);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: DenseMatrix:numNonzeros
 * function() */
int stuartml_linalg_DenseMatrix_numNonzeros (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return moses.countf(self.values, function(x) return x ~= 0 end) */
  const int lc51 = lua_gettop(L);
  lua_pushliteral(L,"countf");
  lua_gettable(L,2);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushcfunction(L,stuartml_linalg_DenseMatrix_numNonzeros_1);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc51);
  assert(lua_gettop(L) == 2);
}


/* name: DenseMatrix.ones
 * function(numRows, numCols) */
int stuartml_linalg_DenseMatrix_ones (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return DenseMatrix.new(numRows, numCols, moses.ones(numRows*numCols)) */
  const int lc54 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  const int lc55 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"ones");
  lua_gettable(L,3);
  lc_mul(L,1,2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc55),LUA_MULTRET);
  return (lua_gettop(L) - lc54);
  assert(lua_gettop(L) == 4);
}


/* name: DenseMatrix:toSparse
 * function() */
int stuartml_linalg_DenseMatrix_toSparse (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local spVals = {} */
  lua_newtable(L);
  assert(lua_gettop(L) == 2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local colPtrs = moses.zeros(self.numCols+1) */
  lua_pushliteral(L,"zeros");
  lua_gettable(L,3);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,1);
  lua_pushnumber(L,1);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local rowIndices = {} */
  lua_newtable(L);
  assert(lua_gettop(L) == 5);

  /* local nnz = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 6);

  /* for j = 0, self.numCols-1 do */
  lua_pushnumber(L,0);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,1);
  lua_pushnumber(L,1);
  lc_sub(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc56_var = lua_tonumber(L,-2);
  const double lc57_limit = lua_tonumber(L,-1);
  const double lc58_step = 1;
  lua_pop(L,2);
  enum { lc59 = 6 };
  while ((((lc58_step > 0) && (lc56_var <= lc57_limit)) || ((lc58_step <= 0) && (lc56_var >= lc57_limit)))) {

    /* internal: local j at index 7 */
    lua_pushnumber(L,lc56_var);

    /* for i = 0, self.numRows-1 do */
    lua_pushnumber(L,0);
    lua_pushliteral(L,"numRows");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_sub(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc60_var = lua_tonumber(L,-2);
    const double lc61_limit = lua_tonumber(L,-1);
    const double lc62_step = 1;
    lua_pop(L,2);
    enum { lc63 = 7 };
    while ((((lc62_step > 0) && (lc60_var <= lc61_limit)) || ((lc62_step <= 0) && (lc60_var >= lc61_limit)))) {

      /* internal: local i at index 8 */
      lua_pushnumber(L,lc60_var);

      /* local v = self.values[self:index(i,j)] */
      lua_pushliteral(L,"values");
      lua_gettable(L,1);
      lua_pushvalue(L,1);
      lua_pushliteral(L,"index");
      lua_gettable(L,-2);
      lua_insert(L,-2);
      lua_pushvalue(L,8);
      lua_pushvalue(L,7);
      lua_call(L,3,1);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      assert(lua_gettop(L) == 9);

      /* if v ~= 0.0 then */
      enum { lc64 = 9 };
      lua_pushnumber(L,0);
      const int lc65 = lua_equal(L,9,-1);
      lua_pop(L,1);
      lua_pushboolean(L,lc65);
      lua_pushboolean(L,!(lua_toboolean(L,-1)));
      lua_remove(L,-2);
      const int lc66 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc66) {

        /* rowIndices[#rowIndices+1] = i */
        lua_pushvalue(L,8);
        const double lc67 = lua_objlen(L,5);
        lua_pushnumber(L,lc67);
        lua_pushnumber(L,1);
        lc_add(L,-2,-1);
        lua_remove(L,-2);
        lua_remove(L,-2);
        lua_insert(L,-2);
        lua_settable(L,5);
        assert(lua_gettop(L) == 9);

        /* spVals[#spVals+1] = v */
        lua_pushvalue(L,9);
        const double lc68 = lua_objlen(L,2);
        lua_pushnumber(L,lc68);
        lua_pushnumber(L,1);
        lc_add(L,-2,-1);
        lua_remove(L,-2);
        lua_remove(L,-2);
        lua_insert(L,-2);
        lua_settable(L,2);
        assert(lua_gettop(L) == 9);

        /* nnz = nnz + 1 */
        lua_pushnumber(L,1);
        lc_add(L,6,-1);
        lua_remove(L,-2);
        lua_replace(L,6);
        assert(lua_gettop(L) == 9);
      }
      lua_settop(L,lc64);
      assert(lua_gettop(L) == 9);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,2);
      lc60_var += lc62_step;
    }
    lua_settop(L,lc63);
    assert(lua_gettop(L) == 7);

    /* colPtrs[j+2] = nnz */
    lua_pushvalue(L,6);
    lua_pushnumber(L,2);
    lc_add(L,7,-1);
    lua_remove(L,-2);
    lua_insert(L,-2);
    lua_settable(L,4);
    assert(lua_gettop(L) == 7);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc56_var += lc58_step;
  }
  lua_settop(L,lc59);
  assert(lua_gettop(L) == 6);

  /* local SparseMatrix = require 'stuart-ml.linalg.SparseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 7);

  /* return SparseMatrix.new(self.numRows, self.numCols, colPtrs, rowIndices, spVals) */
  const int lc69 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,7);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,1);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,1);
  lua_pushvalue(L,4);
  lua_pushvalue(L,5);
  lua_pushvalue(L,2);
  lua_call(L,5,LUA_MULTRET);
  return (lua_gettop(L) - lc69);
  assert(lua_gettop(L) == 7);
}


/* name: DenseMatrix:transpose
 * function() */
int stuartml_linalg_DenseMatrix_transpose (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return DenseMatrix.new(self.numCols, self.numRows, self.values, not self.isTransposed) */
  const int lc75 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,2);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,1);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,1);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_pushliteral(L,"isTransposed");
  lua_gettable(L,1);
  lua_pushboolean(L,!(lua_toboolean(L,-1)));
  lua_remove(L,-2);
  lua_call(L,4,LUA_MULTRET);
  return (lua_gettop(L) - lc75);
  assert(lua_gettop(L) == 2);
}


/* name: DenseMatrix:update
 * function(i, j, v) */
int stuartml_linalg_DenseMatrix_update (lua_State * L) {
  enum { lc_nformalargs = 1 };
  if ((lua_gettop(L) < lc_nformalargs)) {
    lua_settop(L,lc_nformalargs);
  }
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) - lc_nextra == 2);

  /* local nargs = #moses.pack(...) */
  lua_pushliteral(L,"pack");
  lua_gettable(L,(2 + lc_nextra));
  const int lc76 = lua_gettop(L);
  {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
  lua_call(L,(lua_gettop(L) - lc76),1);
  const double lc77 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc77);
  assert(lua_gettop(L) - lc_nextra == 3);

  /* if nargs == 1 then */
  enum { lc78 = 3 };
  lua_pushnumber(L,1);
  const int lc79 = lua_equal(L,(3 + lc_nextra),-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc79);
  const int lc80 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc80) {

    /* return self:updatef(...) */
    const int lc81 = lua_gettop(L);
    lua_pushvalue(L,1);
    const int lc82 = lua_gettop(L);
    lua_pushliteral(L,"updatef");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc82),LUA_MULTRET);
    return (lua_gettop(L) - lc81);
    assert(lua_gettop(L) - lc_nextra == 3);
  }
  else {

    /* else
     * return self:update3(...) */
    const int lc83 = lua_gettop(L);
    lua_pushvalue(L,1);
    const int lc84 = lua_gettop(L);
    lua_pushliteral(L,"update3");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc84),LUA_MULTRET);
    return (lua_gettop(L) - lc83);
    assert(lua_gettop(L) - lc_nextra == 3);
  }
  lua_settop(L,(lc78 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 3);
  return 0;
}


/* name: DenseMatrix:updatef
 * function(f) */
int stuartml_linalg_DenseMatrix_updatef (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for i=1,#self.values do */
  lua_pushnumber(L,1);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  const double lc88 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc88);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc85_var = lua_tonumber(L,-2);
  const double lc86_limit = lua_tonumber(L,-1);
  const double lc87_step = 1;
  lua_pop(L,2);
  enum { lc89 = 2 };
  while ((((lc87_step > 0) && (lc85_var <= lc86_limit)) || ((lc87_step <= 0) && (lc85_var >= lc86_limit)))) {

    /* internal: local i at index 3 */
    lua_pushnumber(L,lc85_var);

    /* self.values[i] = f(self.values[i]) */
    lua_pushvalue(L,2);
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushvalue(L,3);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_call(L,1,1);
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_insert(L,-2);
    lua_pushvalue(L,3);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 3);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc85_var += lc87_step;
  }
  lua_settop(L,lc89);
  assert(lua_gettop(L) == 2);

  /* return self */
  lua_pushvalue(L,1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: DenseMatrix:update3
 * function(i, j, v) */
int stuartml_linalg_DenseMatrix_update3 (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);

  /* self.values[self:index(i, j)] = v */
  lua_pushvalue(L,4);
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  lua_insert(L,-2);
  lua_pushvalue(L,1);
  lua_pushliteral(L,"index");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,2);
  lua_pushvalue(L,3);
  lua_call(L,3,1);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* name: DenseMatrix.zeros
 * function(numRows, numCols) */
int stuartml_linalg_DenseMatrix_zeros (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return DenseMatrix.new(numRows, numCols, moses.zeros(numRows*numCols)) */
  const int lc71 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  const int lc72 = lua_gettop(L);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"zeros");
  lua_gettable(L,3);
  lc_mul(L,1,2);
  lua_call(L,1,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc72),LUA_MULTRET);
  return (lua_gettop(L) - lc71);
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgdmat_map[] = {
  { LSTRKEY("__eq")            , LFUNCVAL (stuartml_linalg_DenseMatrix___eq) },
  { LSTRKEY("_init")           , LFUNCVAL (stuartml_linalg_DenseMatrix__init) },
  { LSTRKEY("asBreeze")        , LFUNCVAL (stuartml_linalg_DenseMatrix_asBreeze) },
  { LSTRKEY("copy")            , LFUNCVAL (stuartml_linalg_DenseMatrix_copy) },
  { LSTRKEY("eye")             , LFUNCVAL (stuartml_linalg_DenseMatrix_eye) },
  { LSTRKEY("foreachActive")   , LFUNCVAL (stuartml_linalg_DenseMatrix_foreachActive) },
  { LSTRKEY("get")             , LFUNCVAL (stuartml_linalg_DenseMatrix_get) },
  { LSTRKEY("index")           , LFUNCVAL (stuartml_linalg_DenseMatrix_index) },
  { LSTRKEY("map")             , LFUNCVAL (stuartml_linalg_DenseMatrix_map) },
  { LSTRKEY("numActives")      , LFUNCVAL (stuartml_linalg_DenseMatrix_numActives) },
  { LSTRKEY("numNonzeros")     , LFUNCVAL (stuartml_linalg_DenseMatrix_numNonzeros) },
  { LSTRKEY("ones")            , LFUNCVAL (stuartml_linalg_DenseMatrix_ones) },
  { LSTRKEY("toSparse")        , LFUNCVAL (stuartml_linalg_DenseMatrix_toSparse) },
  { LSTRKEY("transpose")       , LFUNCVAL (stuartml_linalg_DenseMatrix_transpose) },
  { LSTRKEY("update")          , LFUNCVAL (stuartml_linalg_DenseMatrix_update) },
  { LSTRKEY("updatef")         , LFUNCVAL (stuartml_linalg_DenseMatrix_updatef) },
  { LSTRKEY("update3")         , LFUNCVAL (stuartml_linalg_DenseMatrix_update3) },
  { LSTRKEY("zeros")           , LFUNCVAL (stuartml_linalg_DenseMatrix_zeros) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuartml_linalgdmat_map) },
  { LSTRKEY("_base")           , LRO_ROVAL(stuartml_linalgmat_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuartml_linalgdmat_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuartml_linalg_DenseMatrix_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuartml_linalg_DenseMatrix_new) },

  // inherited (not including __index, _class, _init, classof, and new)
  { LSTRKEY("multiply")        , LFUNCVAL (stuartml_linalg_Matrix_multiply) },
  { LSTRKEY("toArray")         , LFUNCVAL (stuartml_linalg_Matrix_toArray) },
  { LSTRKEY("toString")        , LFUNCVAL (stuartml_linalg_Matrix_toString) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgdmat( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGDMAT, stuartml_linalgdmat_map );
  return 1;
#endif
}
