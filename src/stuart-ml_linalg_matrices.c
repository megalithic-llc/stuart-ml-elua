#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart-ml_linalg_vector.h"

#define MODULE "stuart-ml.linalg.Matrices"


/* name: M.dense
 * function(numRows, numCols, values) */
int stuartml_linalg_Matrices_dense (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return DenseMatrix.new(numRows, numCols, values) */
  const int lc1 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_pushvalue(L,3);
  lua_call(L,3,LUA_MULTRET);
  return (lua_gettop(L) - lc1);
  assert(lua_gettop(L) == 4);
}


/* name: M.diag
 * function() */
int stuartml_linalg_Matrices_diag (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local n = vector:size() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local matrix = DenseMatrix.zeros(n, n) */
  lua_pushliteral(L,"zeros");
  lua_gettable(L,3);
  lua_pushvalue(L,2);
  lua_pushvalue(L,2);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 4);

  /* local values = vector:toArray() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toArray");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* for i=0, n-1 do */
  lua_pushnumber(L,0);
  lua_pushnumber(L,1);
  lc_sub(L,2,-1);
  lua_remove(L,-2);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc2_var = lua_tonumber(L,-2);
  const double lc3_limit = lua_tonumber(L,-1);
  const double lc4_step = 1;
  lua_pop(L,2);
  enum { lc5 = 5 };
  while ((((lc4_step > 0) && (lc2_var <= lc3_limit)) || ((lc4_step <= 0) && (lc2_var >= lc3_limit)))) {

    /* internal: local i at index 6 */
    lua_pushnumber(L,lc2_var);

    /* matrix:update(i, i, values[i+1]) */
    lua_pushvalue(L,4);
    lua_pushliteral(L,"update");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_pushvalue(L,6);
    lua_pushvalue(L,6);
    lua_pushnumber(L,1);
    lc_add(L,6,-1);
    lua_remove(L,-2);
    lua_gettable(L,5);
    lua_call(L,4,0);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc2_var += lc4_step;
  }
  lua_settop(L,lc5);
  assert(lua_gettop(L) == 5);

  /* return matrix */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 5);
}


/* name: M.eye
 * function(n) */
int stuartml_linalg_Matrices_eye (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return DenseMatrix.eye(n) */
  const int lc2 = lua_gettop(L);
  lua_pushliteral(L,"eye");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc2);
  assert(lua_gettop(L) == 2);
}


/* name: M.fromBreeze
 * function() */
int stuartml_linalg_Matrices_fromBreeze (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.fromML
 * function() */
int stuartml_linalg_Matrices_fromML (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.horzcat
 * function() */
int stuartml_linalg_Matrices_horzcat (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.ones
 * function(numRows, numCols) */
int stuartml_linalg_Matrices_ones (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return DenseMatrix.ones(numRows, numCols) */
  const int lc3 = lua_gettop(L);
  lua_pushliteral(L,"ones");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc3);
  assert(lua_gettop(L) == 3);
}


/* name: M.rand
 * function() */
int stuartml_linalg_Matrices_rand (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.randn
 * function() */
int stuartml_linalg_Matrices_randn (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.sparse
 * function (numRows, numCols, colPtrs, rowIndices, values) */
int stuartml_linalg_Matrices_sparse (lua_State * L) {
  enum { lc_nformalargs = 5 };
  lua_settop(L,5);

  /* local SparseMatrix = require 'stuart-ml.linalg.SparseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* return SparseMatrix.new(numRows, numCols, colPtrs, rowIndices, values) */
  const int lc4 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,6);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_pushvalue(L,3);
  lua_pushvalue(L,4);
  lua_pushvalue(L,5);
  lua_call(L,5,LUA_MULTRET);
  return (lua_gettop(L) - lc4);
  assert(lua_gettop(L) == 6);
}


/* name: M.speye
 * function(n) */
int stuartml_linalg_Matrices_speye (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local SparseMatrix = require 'stuart-ml.linalg.SparseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return SparseMatrix.speye(n) */
  const int lc5 = lua_gettop(L);
  lua_pushliteral(L,"speye");
  lua_gettable(L,2);
  lua_pushvalue(L,1);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc5);
  assert(lua_gettop(L) == 2);
}


/* name: M.sprand
 * function() */
int stuartml_linalg_Matrices_sprand (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.vertcat
 * function() */
int stuartml_linalg_Matrices_vertcat (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.sprandn
 * function() */
int stuartml_linalg_Matrices_sprandn (lua_State * L) {
  enum { lc_nformalargs = 0 };
  lua_settop(L,0);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 0);
  return 0;
}


/* name: M.zeros
 * function(numRows, numCols) */
int stuartml_linalg_Matrices_zeros (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local DenseMatrix = require 'stuart-ml.linalg.DenseMatrix' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseMatrix");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return DenseMatrix.zeros(numRows, numCols) */
  const int lc6 = lua_gettop(L);
  lua_pushliteral(L,"zeros");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,2);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc6);
  assert(lua_gettop(L) == 3);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgmats_map[] = {
  { LSTRKEY("dense")               , LFUNCVAL (stuartml_linalg_Matrices_dense) },
  { LSTRKEY("diag")                , LFUNCVAL (stuartml_linalg_Matrices_diag) },
  { LSTRKEY("eye")                 , LFUNCVAL (stuartml_linalg_Matrices_eye) },
  { LSTRKEY("fromBreeze")          , LFUNCVAL (stuartml_linalg_Matrices_fromBreeze) },
  { LSTRKEY("fromML")              , LFUNCVAL (stuartml_linalg_Matrices_fromML) },
  { LSTRKEY("horzcat")             , LFUNCVAL (stuartml_linalg_Matrices_horzcat) },
  { LSTRKEY("ones")                , LFUNCVAL (stuartml_linalg_Matrices_ones) },
  { LSTRKEY("rand")                , LFUNCVAL (stuartml_linalg_Matrices_rand) },
  { LSTRKEY("randn")               , LFUNCVAL (stuartml_linalg_Matrices_randn) },
  { LSTRKEY("sparse")              , LFUNCVAL (stuartml_linalg_Matrices_sparse) },
  { LSTRKEY("speye")               , LFUNCVAL (stuartml_linalg_Matrices_speye) },
  { LSTRKEY("sprand")              , LFUNCVAL (stuartml_linalg_Matrices_sprand) },
  { LSTRKEY("sprandn")             , LFUNCVAL (stuartml_linalg_Matrices_sprandn) },
  { LSTRKEY("vertcat")             , LFUNCVAL (stuartml_linalg_Matrices_vertcat) },
  { LSTRKEY("zeros")               , LFUNCVAL (stuartml_linalg_Matrices_zeros) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgmats( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGMATS, stuartml_linalgmats_map );
  return 1;
#endif
}
