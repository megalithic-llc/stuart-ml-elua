#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart-ml.linalg.Matrix"


/* name: Matrix:_init
 * function() */
int stuartml_linalg_Matrix__init (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* -- Flag that keeps track whether the matrix is transposed or not. False by default.
   * self.isTransposed = false */
  lua_pushboolean(L,0);
  lua_pushliteral(L,"isTransposed");
  lua_insert(L,-2);
  lua_settable(L,1);
  return 0;
}


/* name: Matrix:__eq
 * function(other) */
int stuartml_linalg_Matrix___eq (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return self:toSparse() == other:toSparse() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"toSparse");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"toSparse");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  const int lc1 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc1);
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: M.classof
 * function(obj) */
int stuartml_linalg_Matrix_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: Matrix:multiply
 * function() */
int stuartml_linalg_Matrix_multiply (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


/* name: M.new
 * function(...) */
int stuartml_linalg_Matrix_new (lua_State * L) {
  return stuart_class_shared_new(L, MODULE);
}


/* function(i, j, v) */
int stuartml_linalg_Matrix_toArray_1 (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* newArray[1 + j * self.numRows + i] = v */
  lua_pushvalue(L,3);
  lc_getupvalue(L,lua_upvalueindex(1),0,2);
  lua_insert(L,-2);
  lua_pushnumber(L,1);
  lc_getupvalue(L,lua_upvalueindex(1),1,1);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_mul(L,2,-1);
  lua_remove(L,-2);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_add(L,-1,1);
  lua_remove(L,-2);
  lua_insert(L,-2);
  lua_settable(L,-3);
  lua_pop(L,1);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: Matrix:toArray
 * function() */
int stuartml_linalg_Matrix_toArray (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc2 = 2 };
  assert((lua_gettop(L) == lc2));
  lua_pushvalue(L,1);
  lua_rawseti(L,-2,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local newArray = moses.zeros(self.numRows + self.numCols) */
  lc_newclosuretable(L,lc2);
  enum { lc3 = 4 };
  assert((lua_gettop(L) == lc3));
  lua_pushliteral(L,"zeros");
  lua_gettable(L,3);
  lc_getupvalue(L,lc2,0,1);
  lua_pushliteral(L,"numRows");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_getupvalue(L,lc2,0,1);
  lua_pushliteral(L,"numCols");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lua_call(L,1,1);
  lua_rawseti(L,lc3,2);
  assert(lua_gettop(L) == 4);

  /* self:foreachActive(function(i, j, v)
   *     newArray[1 + j * self.numRows + i] = v
   *   end) */
  lc_getupvalue(L,lc3,1,1);
  lua_pushliteral(L,"foreachActive");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_pushvalue(L,lc3);
  lua_pushcclosure(L,stuartml_linalg_Matrix_toArray_1,1);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 4);

  /* return newArray */
  lc_getupvalue(L,lc3,0,2);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: Matrix:toString
 * function() */
int stuartml_linalg_Matrix_toString (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* error('NIY') */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"NIY");
  lua_call(L,1,0);
  assert(lua_gettop(L) == 1);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgmat_map[] = {
  { LSTRKEY("_init")           , LFUNCVAL (stuartml_linalg_Matrix__init) },
  { LSTRKEY("__eq")            , LFUNCVAL (stuartml_linalg_Matrix___eq) },
  { LSTRKEY("multiply")        , LFUNCVAL (stuartml_linalg_Matrix_multiply) },
  { LSTRKEY("toArray")         , LFUNCVAL (stuartml_linalg_Matrix_toArray) },
  { LSTRKEY("toString")        , LFUNCVAL (stuartml_linalg_Matrix_toString) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuartml_linalgmat_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuartml_linalgmat_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuartml_linalg_Matrix_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuartml_linalg_Matrix_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgmat( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGMAT, stuartml_linalgmat_map );
  return 1;
#endif
}
