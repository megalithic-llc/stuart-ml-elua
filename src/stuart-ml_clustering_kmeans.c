#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart-ml.clustering.KMeans"


/* name: KMeans._find
 * function(array, value) */
int stuartml_clustering_KMeans__find (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* for i = 1, #array do */
  lua_pushnumber(L,1);
  const double lc6 = lua_objlen(L,1);
  lua_pushnumber(L,lc6);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc3_var = lua_tonumber(L,-2);
  const double lc4_limit = lua_tonumber(L,-1);
  const double lc5_step = 1;
  lua_pop(L,2);
  enum { lc7 = 3 };
  while ((((lc5_step > 0) && (lc3_var <= lc4_limit)) || ((lc5_step <= 0) && (lc3_var >= lc4_limit)))) {

    /* internal: local i at index 4 */
    lua_pushnumber(L,lc3_var);

    /* if moses.isEqual(array[i], value, true) then */
    enum { lc8 = 4 };
    lua_pushliteral(L,"isEqual");
    lua_gettable(L,3);
    lua_pushvalue(L,4);
    lua_gettable(L,1);
    lua_pushvalue(L,2);
    lua_pushboolean(L,1);
    lua_call(L,3,1);
    const int lc9 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc9) {

      /* return i */
      lua_pushvalue(L,4);
      return 1;
      assert(lua_gettop(L) == 4);
    }
    lua_settop(L,lc8);
    assert(lua_gettop(L) == 4);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc3_var += lc5_step;
  }
  lua_settop(L,lc7);
  assert(lua_gettop(L) == 3);
  return 0;
}


/* name: KMeans._unique
 * function(array) */
int stuartml_clustering_KMeans__unique (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local KMeans = require 'stuart-ml.clustering.KMeans' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local ret = {} */
  lua_newtable(L);
  assert(lua_gettop(L) == 3);

  /* for i = 1, #array do */
  lua_pushnumber(L,1);
  const double lc13 = lua_objlen(L,1);
  lua_pushnumber(L,lc13);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc10_var = lua_tonumber(L,-2);
  const double lc11_limit = lua_tonumber(L,-1);
  const double lc12_step = 1;
  lua_pop(L,2);
  enum { lc14 = 3 };
  while ((((lc12_step > 0) && (lc10_var <= lc11_limit)) || ((lc12_step <= 0) && (lc10_var >= lc11_limit)))) {

    /* internal: local i at index 4 */
    lua_pushnumber(L,lc10_var);

    /* if not KMeans._find(ret, array[i]) then */
    enum { lc15 = 4 };
    lua_pushliteral(L,"_find");
    lua_gettable(L,2);
    lua_pushvalue(L,3);
    lua_pushvalue(L,4);
    lua_gettable(L,1);
    lua_call(L,2,1);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
    const int lc16 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc16) {

      /* ret[#ret+1] = array[i] */
      lua_pushvalue(L,4);
      lua_gettable(L,1);
      const double lc17 = lua_objlen(L,3);
      lua_pushnumber(L,lc17);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_insert(L,-2);
      lua_settable(L,3);
      assert(lua_gettop(L) == 4);
    }
    lua_settop(L,lc15);
    assert(lua_gettop(L) == 4);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc10_var += lc12_step;
  }
  lua_settop(L,lc14);
  assert(lua_gettop(L) == 3);

  /* return ret */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 3);
}


/* name: M.classof
 * function(obj) */
int stuartml_clustering_KMeans_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: KMeans.fastSquaredDistance
 * function(vectorWithNorm1, vectorWithNorm2) */
int stuartml_clustering_KMeans_fastSquaredDistance (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local MLUtils = require 'stuart-ml.util.MLUtils' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.util.MLUtils");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* return MLUtils.fastSquaredDistance(vectorWithNorm1.vector, vectorWithNorm1.norm, vectorWithNorm2.vector, vectorWithNorm2.norm) */
  const int lc18 = lua_gettop(L);
  lua_pushliteral(L,"fastSquaredDistance");
  lua_gettable(L,3);
  lua_pushliteral(L,"vector");
  lua_gettable(L,1);
  lua_pushliteral(L,"norm");
  lua_gettable(L,1);
  lua_pushliteral(L,"vector");
  lua_gettable(L,2);
  lua_pushliteral(L,"norm");
  lua_gettable(L,2);
  lua_call(L,4,LUA_MULTRET);
  return (lua_gettop(L) - lc18);
  assert(lua_gettop(L) == 3);
}


/* name: KMeans.findClosest
 * function(centers, point) */
int stuartml_clustering_KMeans_findClosest (lua_State * L) {
  lua_checkstack(L,22);
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local bestDistance = math.huge */
  lua_getfield(L,LUA_ENVIRONINDEX,"math");
  lua_pushliteral(L,"huge");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 3);

  /* local bestIndex = 1 */
  lua_pushnumber(L,1);
  assert(lua_gettop(L) == 4);

  /* local KMeans = require 'stuart-ml.clustering.KMeans' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* for i,center in ipairs(centers) do
   * internal: local f, s, var = explist */
  enum { lc19 = 5 };
  lua_getfield(L,LUA_ENVIRONINDEX,"ipairs");
  lua_pushvalue(L,1);
  lua_call(L,1,3);
  while (1) {

    /* internal: local var_1, ..., var_n = f(s, var)
     *           if var_1 == nil then break end
     *           var = var_1 */
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_pushvalue(L,-3);
    lua_call(L,2,2);
    if (lua_isnil(L,-2)) {
      break;
    }
    lua_pushvalue(L,-2);
    lua_replace(L,-4);

    /* internal: local i with idx 9
     * internal: local center with idx 10 */


    /* local lowerBoundOfSqDist = center.norm - point.norm */
    lua_pushliteral(L,"norm");
    lua_gettable(L,10);
    lua_pushliteral(L,"norm");
    lua_gettable(L,2);
    lc_sub(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    assert(lua_gettop(L) == 11);

    /* lowerBoundOfSqDist = lowerBoundOfSqDist * lowerBoundOfSqDist */
    lc_mul(L,11,11);
    lua_replace(L,11);
    assert(lua_gettop(L) == 11);

    /* if lowerBoundOfSqDist < bestDistance then */
    enum { lc20 = 11 };
    const int lc21 = lua_lessthan(L,11,3);
    lua_pushboolean(L,lc21);
    const int lc22 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc22) {

      /* local distance = KMeans.fastSquaredDistance(center, point) */
      lua_pushliteral(L,"fastSquaredDistance");
      lua_gettable(L,5);
      lua_pushvalue(L,10);
      lua_pushvalue(L,2);
      lua_call(L,2,1);
      assert(lua_gettop(L) == 12);

      /* if distance < bestDistance then */
      enum { lc23 = 12 };
      const int lc24 = lua_lessthan(L,12,3);
      lua_pushboolean(L,lc24);
      const int lc25 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc25) {

        /* bestDistance = distance */
        lua_pushvalue(L,12);
        lua_replace(L,3);
        assert(lua_gettop(L) == 12);

        /* bestIndex = i */
        lua_pushvalue(L,9);
        lua_replace(L,4);
        assert(lua_gettop(L) == 12);
      }
      lua_settop(L,lc23);
      assert(lua_gettop(L) == 12);
    }
    lua_settop(L,lc20);
    assert(lua_gettop(L) == 11);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,3);
  }
  lua_settop(L,lc19);
  assert(lua_gettop(L) == 5);

  /* return bestIndex, bestDistance */
  lua_pushvalue(L,4);
  lua_pushvalue(L,3);
  return 2;
  assert(lua_gettop(L) == 5);
}


/* name: M.new
 * function(...) */
int stuartml_clustering_KMeans_new (lua_State * L) {
  return stuart_class_shared_new(L, MODULE);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_clustk_map[] = {
  { LSTRKEY("_find")              , LFUNCVAL (stuartml_clustering_KMeans__find) },
  { LSTRKEY("_unique")            , LFUNCVAL (stuartml_clustering_KMeans__unique) },
  { LSTRKEY("fastSquaredDistance"), LFUNCVAL (stuartml_clustering_KMeans_fastSquaredDistance) },
  { LSTRKEY("findClosest")        , LFUNCVAL (stuartml_clustering_KMeans_findClosest) },

  // class framework
  { LSTRKEY("__index")            , LRO_ROVAL(stuartml_clustk_map) },
  { LSTRKEY("_class")             , LRO_ROVAL(stuartml_clustk_map) },
  { LSTRKEY("classof")            , LFUNCVAL (stuartml_clustering_KMeans_classof) },
  { LSTRKEY("new")                , LFUNCVAL (stuartml_clustering_KMeans_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_clustk( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_CLUSTK, stuartml_clustk_map );
  return 1;
#endif
}
