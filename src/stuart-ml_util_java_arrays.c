#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart-ml.util.java.arrays"


/* name: M.binarySearch
 * function(a, fromIndex, toIndex, key) */
int stuartml_util_java_arrays_binarySearch (lua_State * L) {
  enum { lc_nformalargs = 4 };
  lua_settop(L,4);

  /* fromIndex = fromIndex or 0 */
  lua_pushvalue(L,2);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushnumber(L,0);
  }
  lua_replace(L,2);
  assert(lua_gettop(L) == 4);

  /* assert(fromIndex >= 0, 'fromIndex must be >= 0') */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnumber(L,0);
  const int lc1 = lc_le(L,-1,2);
  lua_pop(L,1);
  lua_pushboolean(L,lc1);
  lua_pushliteral(L,"fromIndex must be >= 0");
  lua_call(L,2,0);
  assert(lua_gettop(L) == 4);

  /* toIndex = toIndex or nil */
  lua_pushvalue(L,3);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushnil(L);
  }
  lua_replace(L,3);
  assert(lua_gettop(L) == 4);

  /* if toIndex == nil then */
  enum { lc2 = 4 };
  lua_pushnil(L);
  const int lc3 = lua_equal(L,3,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc3);
  const int lc4 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc4) {

    /* toIndex = #a */
    const double lc5 = lua_objlen(L,1);
    lua_pushnumber(L,lc5);
    lua_replace(L,3);
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc2);
  assert(lua_gettop(L) == 4);

  /* while fromIndex < toIndex do */
  enum { lc6 = 4 };
  while (1) {
    const int lc7 = lua_lessthan(L,2,3);
    lua_pushboolean(L,lc7);
    if (!(lua_toboolean(L,-1))) {
      break;
    }
    lua_pop(L,1);

    /* local mid = math.floor((fromIndex+toIndex) / 2) */
    lua_getfield(L,LUA_ENVIRONINDEX,"math");
    lua_pushliteral(L,"floor");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lc_add(L,2,3);
    lua_pushnumber(L,2);
    lc_div(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_call(L,1,1);
    assert(lua_gettop(L) == 5);

    /* local midVal = a[mid+1] */
    lua_pushnumber(L,1);
    lc_add(L,5,-1);
    lua_remove(L,-2);
    lua_gettable(L,1);
    assert(lua_gettop(L) == 6);

    /* if midVal < key then */
    enum { lc8 = 6 };
    const int lc9 = lua_lessthan(L,6,4);
    lua_pushboolean(L,lc9);
    const int lc10 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc10) {

      /* fromIndex = mid+1 */
      lua_pushnumber(L,1);
      lc_add(L,5,-1);
      lua_remove(L,-2);
      lua_replace(L,2);
      assert(lua_gettop(L) == 6);
    }
    else {

      /* elseif midVal > key then */
      enum { lc11 = 6 };
      const int lc12 = lua_lessthan(L,4,6);
      lua_pushboolean(L,lc12);
      const int lc13 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc13) {

        /* toIndex = mid */
        lua_pushvalue(L,5);
        lua_replace(L,3);
        assert(lua_gettop(L) == 6);
      }
      else {

        /* else
         * return mid */
        lua_pushvalue(L,5);
        return 1;
        assert(lua_gettop(L) == 6);
      }
      lua_settop(L,lc11);
    }
    lua_settop(L,lc8);
    assert(lua_gettop(L) == 6);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,2);
  }
  lua_settop(L,lc6);
  assert(lua_gettop(L) == 4);

  /* return -fromIndex */
  lc_unm(L,2);
  return 1;
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_util_javaa_map[] = {
  { LSTRKEY("binarySearch")      , LFUNCVAL (stuartml_util_java_arrays_binarySearch) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_util_javaa( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_UTIL_JAVAA, stuartml_util_java_a_map );
  return 1;
#endif
}
