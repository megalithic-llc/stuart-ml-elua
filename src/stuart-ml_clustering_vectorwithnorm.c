#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart-ml.clustering.VectorWithNorm"


/* name: VectorWithNorm:_init
 * function(arg1, norm) */
int stuartml_clustering_VectorWithNorm__init (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local Vector = require 'stuart-ml.linalg.Vector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.Vector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local Vectors = require 'stuart-ml.linalg.Vectors' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.Vectors");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* local class = require 'stuart.class' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* if class.istype(arg1, Vector) then */
  enum { lc1 = 6 };
  lua_pushliteral(L,"istype");
  lua_gettable(L,6);
  lua_pushvalue(L,2);
  lua_pushvalue(L,4);
  lua_call(L,2,1);
  const int lc2 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc2) {

    /* self.vector = arg1 */
    lua_pushvalue(L,2);
    lua_pushliteral(L,"vector");
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 6);
  }
  else {

    /* else
     * -- arg1 is a table
     * self.vector = Vectors.dense(arg1) */
    lua_pushliteral(L,"dense");
    lua_gettable(L,5);
    lua_pushvalue(L,2);
    lua_call(L,1,1);
    lua_pushliteral(L,"vector");
    lua_insert(L,-2);
    lua_settable(L,1);
    assert(lua_gettop(L) == 6);
  }
  lua_settop(L,lc1);
  assert(lua_gettop(L) == 6);

  /* self.norm = norm or Vectors.norm(self.vector, 2.0) */
  lua_pushvalue(L,3);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushliteral(L,"norm");
    lua_gettable(L,5);
    lua_pushliteral(L,"vector");
    lua_gettable(L,1);
    lua_pushnumber(L,2);
    lua_call(L,2,1);
  }
  lua_pushliteral(L,"norm");
  lua_insert(L,-2);
  lua_settable(L,1);
  assert(lua_gettop(L) == 6);
  return 0;
}


/* name: VectorWithNorm.__eq
 * function(a, b) */
int stuartml_clustering_VectorWithNorm___eq (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* return a.vector == b.vector and a.norm == b.norm */
  lua_pushliteral(L,"vector");
  lua_gettable(L,1);
  lua_pushliteral(L,"vector");
  lua_gettable(L,2);
  const int lc3 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc3);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"norm");
    lua_gettable(L,1);
    lua_pushliteral(L,"norm");
    lua_gettable(L,2);
    const int lc4 = lua_equal(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc4);
  }
  return 1;
  assert(lua_gettop(L) == 2);
}


/* name: VectorWithNorm:__tostring
 * function() */
int stuartml_clustering_VectorWithNorm___tostring (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* return '(' .. tostring(self.vector) .. ',' .. self.norm .. ')' */
  lua_pushliteral(L,"(");
  lua_getfield(L,LUA_ENVIRONINDEX,"tostring");
  lua_pushliteral(L,"vector");
  lua_gettable(L,1);
  lua_call(L,1,1);
  lua_pushliteral(L,",");
  lua_pushliteral(L,"norm");
  lua_gettable(L,1);
  lua_pushliteral(L,")");
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  return 1;
  assert(lua_gettop(L) == 1);
}


/* name: M.classof
 * function(obj) */
int stuartml_clustering_VectorWithNorm_classof (lua_State * L) {
  return stuart_class_shared_classof(L, MODULE);
}


/* name: M.new
 * function(...) */
int stuartml_clustering_VectorWithNorm_new (lua_State * L) {
  return stuart_class_shared_new(L, MODULE);
}


/* name: VectorWithNorm:toDense
 * function() */
int stuartml_clustering_VectorWithNorm_toDense (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local VectorWithNorm = require 'stuart-ml.clustering.VectorWithNorm' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* return VectorWithNorm.new(self.vector:toDense(), self.norm) */
  const int lc5 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,2);
  lua_pushliteral(L,"vector");
  lua_gettable(L,1);
  lua_pushliteral(L,"toDense");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L,"norm");
  lua_gettable(L,1);
  lua_call(L,2,LUA_MULTRET);
  return (lua_gettop(L) - lc5);
  assert(lua_gettop(L) == 2);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_clustvwn_map[] = {
  { LSTRKEY("__eq")      , LFUNCVAL (stuartml_clustering_VectorWithNorm___eq) },
  { LSTRKEY("__tostring")      , LFUNCVAL (stuartml_clustering_VectorWithNorm___tostring) },
  { LSTRKEY("_init")           , LFUNCVAL (stuartml_clustering_VectorWithNorm__init) },
  { LSTRKEY("toDense")     , LFUNCVAL (stuartml_clustering_VectorWithNorm_toDense) },

  // class framework
  { LSTRKEY("__index")         , LRO_ROVAL(stuartml_clustvwn_map) },
  { LSTRKEY("_class")          , LRO_ROVAL(stuartml_clustvwn_map) },
  { LSTRKEY("classof")         , LFUNCVAL (stuartml_clustering_VectorWithNorm_classof) },
  { LSTRKEY("new")             , LFUNCVAL (stuartml_clustering_VectorWithNorm_new) },

  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_clustvwn( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_CLUSTVWN, stuartml_clustvwn_map );
  return 1;
#endif
}
