#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"

#define MODULE "stuart-ml.linalg.BLAS"


/* name: M.axpy
 * function(a, vectorX, vectorY) */
int stuartml_linalg_BLAS_axpy (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local class = require 'stuart.class' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local istype = class.istype */
  lua_pushliteral(L,"istype");
  lua_gettable(L,4);
  assert(lua_gettop(L) == 5);

  /* local Vector = require 'stuart-ml.linalg.Vector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.Vector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* assert(istype(vectorX, Vector)) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const int lc1 = lua_gettop(L);
  lua_pushvalue(L,5);
  lua_pushvalue(L,2);
  lua_pushvalue(L,6);
  lua_call(L,2,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc1),0);
  assert(lua_gettop(L) == 6);

  /* assert(istype(vectorY, Vector)) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  const int lc2 = lua_gettop(L);
  lua_pushvalue(L,5);
  lua_pushvalue(L,3);
  lua_pushvalue(L,6);
  lua_call(L,2,LUA_MULTRET);
  lua_call(L,(lua_gettop(L) - lc2),0);
  assert(lua_gettop(L) == 6);

  /* assert(vectorX:size() == vectorY:size()) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushvalue(L,2);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,3);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  const int lc3 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc3);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 6);

  /* local DenseVector = require 'stuart-ml.linalg.DenseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 7);

  /* local M = require 'stuart-ml.linalg.BLAS' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* if istype(vectorY,DenseVector) then */
  enum { lc4 = 8 };
  lua_pushvalue(L,5);
  lua_pushvalue(L,3);
  lua_pushvalue(L,7);
  lua_call(L,2,1);
  const int lc5 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc5) {

    /* local SparseVector = require 'stuart-ml.linalg.SparseVector' */
    lua_getfield(L,LUA_ENVIRONINDEX,"require");
    lua_pushliteral(L,"stuart-ml.linalg.SparseVector");
    lua_call(L,1,1);
    assert(lua_gettop(L) == 9);

    /* if istype(vectorX,SparseVector) then */
    enum { lc6 = 9 };
    lua_pushvalue(L,5);
    lua_pushvalue(L,2);
    lua_pushvalue(L,9);
    lua_call(L,2,1);
    const int lc7 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc7) {

      /* return M.axpy_sparse_dense(a,vectorX,vectorY) */
      const int lc8 = lua_gettop(L);
      lua_pushliteral(L,"axpy_sparse_dense");
      lua_gettable(L,8);
      lua_pushvalue(L,1);
      lua_pushvalue(L,2);
      lua_pushvalue(L,3);
      lua_call(L,3,LUA_MULTRET);
      return (lua_gettop(L) - lc8);
      assert(lua_gettop(L) == 9);
    }
    else {

      /* elseif istype(vectorX,DenseVector) then */
      enum { lc9 = 9 };
      lua_pushvalue(L,5);
      lua_pushvalue(L,2);
      lua_pushvalue(L,7);
      lua_call(L,2,1);
      const int lc10 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc10) {

        /* return M.axpy_sparse_dense(a,vectorX:toSparse(),vectorY) */
        const int lc11 = lua_gettop(L);
        lua_pushliteral(L,"axpy_sparse_dense");
        lua_gettable(L,8);
        lua_pushvalue(L,1);
        lua_pushvalue(L,2);
        lua_pushliteral(L,"toSparse");
        lua_gettable(L,-2);
        lua_insert(L,-2);
        lua_call(L,1,1);
        lua_pushvalue(L,3);
        lua_call(L,3,LUA_MULTRET);
        return (lua_gettop(L) - lc11);
        assert(lua_gettop(L) == 9);
      }
      else {

        /* else
         * error('axpy doesn\'t support vectorX type ' .. vectorX.class) */
        lua_getfield(L,LUA_ENVIRONINDEX,"error");
        lua_pushliteral(L,"axpy doesn't support vectorX type ");
        lua_pushliteral(L,"class");
        lua_gettable(L,2);
        lua_concat(L,2);
        lua_call(L,1,0);
        assert(lua_gettop(L) == 9);
      }
      lua_settop(L,lc9);
    }
    lua_settop(L,lc6);
    assert(lua_gettop(L) == 9);
  }
  lua_settop(L,lc4);
  assert(lua_gettop(L) == 8);

  /* error('axpy only supports adding to a DenseVector but got type ' .. class.type(vectorY)) */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"axpy only supports adding to a DenseVector but got type ");
  lua_pushliteral(L,"type");
  lua_gettable(L,4);
  lua_pushvalue(L,3);
  lua_call(L,1,1);
  lua_concat(L,2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 8);
  return 0;
}


/* name: M.axpy_sparse_dense
 * function(a, x, y) */
int stuartml_linalg_BLAS_axpy_sparse_dense (lua_State * L) {
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local nnz = #x.indices */
  lua_pushliteral(L,"indices");
  lua_gettable(L,2);
  const double lc13 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc13);
  assert(lua_gettop(L) == 4);

  /* if a == 1.0 then */
  enum { lc14 = 4 };
  lua_pushnumber(L,1);
  const int lc15 = lua_equal(L,1,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc15);
  const int lc16 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc16) {

    /* for k=1,nnz do */
    lua_pushnumber(L,1);
    if (!((lua_isnumber(L,-1) && lua_isnumber(L,4)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc17_var = lua_tonumber(L,-1);
    const double lc18_limit = lua_tonumber(L,4);
    const double lc19_step = 1;
    lua_pop(L,1);
    enum { lc20 = 4 };
    while ((((lc19_step > 0) && (lc17_var <= lc18_limit)) || ((lc19_step <= 0) && (lc17_var >= lc18_limit)))) {

      /* internal: local k at index 5 */
      lua_pushnumber(L,lc17_var);

      /* y.values[x.indices[k]+1] = y.values[x.indices[k]+1] + x.values[k] */
      lua_pushliteral(L,"values");
      lua_gettable(L,3);
      lua_pushliteral(L,"indices");
      lua_gettable(L,2);
      lua_pushvalue(L,5);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"values");
      lua_gettable(L,2);
      lua_pushvalue(L,5);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"values");
      lua_gettable(L,3);
      lua_insert(L,-2);
      lua_pushliteral(L,"indices");
      lua_gettable(L,2);
      lua_pushvalue(L,5);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_insert(L,-2);
      lua_settable(L,-3);
      lua_pop(L,1);
      assert(lua_gettop(L) == 5);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,1);
      lc17_var += lc19_step;
    }
    lua_settop(L,lc20);
    assert(lua_gettop(L) == 4);
  }
  else {

    /* else
     * for k=1,nnz do */
    lua_pushnumber(L,1);
    if (!((lua_isnumber(L,-1) && lua_isnumber(L,4)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc21_var = lua_tonumber(L,-1);
    const double lc22_limit = lua_tonumber(L,4);
    const double lc23_step = 1;
    lua_pop(L,1);
    enum { lc24 = 4 };
    while ((((lc23_step > 0) && (lc21_var <= lc22_limit)) || ((lc23_step <= 0) && (lc21_var >= lc22_limit)))) {

      /* internal: local k at index 5 */
      lua_pushnumber(L,lc21_var);

      /* y.values[x.indices[k]+1] = y.values[x.indices[k]+1] + a * x.values[k] */
      lua_pushliteral(L,"values");
      lua_gettable(L,3);
      lua_pushliteral(L,"indices");
      lua_gettable(L,2);
      lua_pushvalue(L,5);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"values");
      lua_gettable(L,2);
      lua_pushvalue(L,5);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lc_mul(L,1,-1);
      lua_remove(L,-2);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"values");
      lua_gettable(L,3);
      lua_insert(L,-2);
      lua_pushliteral(L,"indices");
      lua_gettable(L,2);
      lua_pushvalue(L,5);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushnumber(L,1);
      lc_add(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_insert(L,-2);
      lua_settable(L,-3);
      lua_pop(L,1);
      assert(lua_gettop(L) == 5);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,1);
      lc21_var += lc23_step;
    }
    lua_settop(L,lc24);
    assert(lua_gettop(L) == 4);
  }
  lua_settop(L,lc14);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* name: M.dot
 * function(x, y) */
int stuartml_linalg_BLAS_dot (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* assert(x:size() == y:size()) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  const int lc24 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc24);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 2);

  /* local class = require 'stuart.class' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local istype = class.istype */
  lua_pushliteral(L,"istype");
  lua_gettable(L,3);
  assert(lua_gettop(L) == 4);

  /* local DenseVector = require 'stuart-ml.linalg.DenseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* local M = require 'stuart-ml.linalg.BLAS' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* if istype(x,DenseVector) and istype(y,DenseVector) then */
  enum { lc25 = 6 };
  lua_pushvalue(L,4);
  lua_pushvalue(L,1);
  lua_pushvalue(L,5);
  lua_call(L,2,1);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,4);
    lua_pushvalue(L,2);
    lua_pushvalue(L,5);
    lua_call(L,2,1);
  }
  const int lc26 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc26) {

    /* return M.dot_sparse_dense(x:toSparse(), y) */
    const int lc27 = lua_gettop(L);
    lua_pushliteral(L,"dot_sparse_dense");
    lua_gettable(L,6);
    lua_pushvalue(L,1);
    lua_pushliteral(L,"toSparse");
    lua_gettable(L,-2);
    lua_insert(L,-2);
    lua_call(L,1,1);
    lua_pushvalue(L,2);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc27);
    assert(lua_gettop(L) == 6);
  }
  lua_settop(L,lc25);
  assert(lua_gettop(L) == 6);

  /* local SparseVector = require 'stuart-ml.linalg.SparseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 7);

  /* if istype(x,SparseVector) and istype(y,DenseVector) then */
  enum { lc28 = 7 };
  lua_pushvalue(L,4);
  lua_pushvalue(L,1);
  lua_pushvalue(L,7);
  lua_call(L,2,1);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushvalue(L,4);
    lua_pushvalue(L,2);
    lua_pushvalue(L,5);
    lua_call(L,2,1);
  }
  const int lc29 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc29) {

    /* return M.dot_sparse_dense(x, y) */
    const int lc30 = lua_gettop(L);
    lua_pushliteral(L,"dot_sparse_dense");
    lua_gettable(L,6);
    lua_pushvalue(L,1);
    lua_pushvalue(L,2);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc30);
    assert(lua_gettop(L) == 7);
  }
  else {

    /* elseif istype(x,DenseVector) and istype(y,SparseVector) then */
    enum { lc31 = 7 };
    lua_pushvalue(L,4);
    lua_pushvalue(L,1);
    lua_pushvalue(L,5);
    lua_call(L,2,1);
    if (lua_toboolean(L,-1)) {
      lua_pop(L,1);
      lua_pushvalue(L,4);
      lua_pushvalue(L,2);
      lua_pushvalue(L,7);
      lua_call(L,2,1);
    }
    const int lc32 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc32) {

      /* return M.dot_sparse_dense(y, x) */
      const int lc33 = lua_gettop(L);
      lua_pushliteral(L,"dot_sparse_dense");
      lua_gettable(L,6);
      lua_pushvalue(L,2);
      lua_pushvalue(L,1);
      lua_call(L,2,LUA_MULTRET);
      return (lua_gettop(L) - lc33);
      assert(lua_gettop(L) == 7);
    }
    else {

      /* elseif istype(x,SparseVector) and istype(y,SparseVector) then */
      enum { lc34 = 7 };
      lua_pushvalue(L,4);
      lua_pushvalue(L,1);
      lua_pushvalue(L,7);
      lua_call(L,2,1);
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lua_pushvalue(L,4);
        lua_pushvalue(L,2);
        lua_pushvalue(L,7);
        lua_call(L,2,1);
      }
      const int lc35 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc35) {

        /* return M.dot_sparse_sparse(x, y) */
        const int lc36 = lua_gettop(L);
        lua_pushliteral(L,"dot_sparse_sparse");
        lua_gettable(L,6);
        lua_pushvalue(L,1);
        lua_pushvalue(L,2);
        lua_call(L,2,LUA_MULTRET);
        return (lua_gettop(L) - lc36);
        assert(lua_gettop(L) == 7);
      }
      else {

        /* else
         * error(string.format("dot doesn't support (%s,%s)", class.type(x), class.type(y))) */
        lua_getfield(L,LUA_ENVIRONINDEX,"error");
        const int lc37 = lua_gettop(L);
        lua_getfield(L,LUA_ENVIRONINDEX,"string");
        lua_pushliteral(L,"format");
        lua_gettable(L,-2);
        lua_remove(L,-2);
        const int lc38 = lua_gettop(L);
        lua_pushliteral(L,"dot doesn't support (%s,%s)");
        lua_pushliteral(L,"type");
        lua_gettable(L,3);
        lua_pushvalue(L,1);
        lua_call(L,1,1);
        lua_pushliteral(L,"type");
        lua_gettable(L,3);
        lua_pushvalue(L,2);
        lua_call(L,1,LUA_MULTRET);
        lua_call(L,(lua_gettop(L) - lc38),LUA_MULTRET);
        lua_call(L,(lua_gettop(L) - lc37),0);
        assert(lua_gettop(L) == 7);
      }
      lua_settop(L,lc34);
    }
    lua_settop(L,lc31);
  }
  lua_settop(L,lc28);
  assert(lua_gettop(L) == 7);
  return 0;
}


/* name: M.dot_sparse_dense
 * function(x, y) */
int stuartml_linalg_BLAS_dot_sparse_dense (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local nnz = #x.indices */
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  const double lc40 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc40);
  assert(lua_gettop(L) == 3);

  /* local sum = 0.0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 4);

  /* for k=1,nnz do */
  lua_pushnumber(L,1);
  if (!((lua_isnumber(L,-1) && lua_isnumber(L,3)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc41_var = lua_tonumber(L,-1);
  const double lc42_limit = lua_tonumber(L,3);
  const double lc43_step = 1;
  lua_pop(L,1);
  enum { lc44 = 4 };
  while ((((lc43_step > 0) && (lc41_var <= lc42_limit)) || ((lc43_step <= 0) && (lc41_var >= lc42_limit)))) {

    /* internal: local k at index 5 */
    lua_pushnumber(L,lc41_var);

    /* sum = sum + x.values[k] * y.values[x.indices[k]+1] */
    lua_pushliteral(L,"values");
    lua_gettable(L,1);
    lua_pushvalue(L,5);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushliteral(L,"values");
    lua_gettable(L,2);
    lua_pushliteral(L,"indices");
    lua_gettable(L,1);
    lua_pushvalue(L,5);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushnumber(L,1);
    lc_add(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lc_mul(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lc_add(L,4,-1);
    lua_remove(L,-2);
    lua_replace(L,4);
    assert(lua_gettop(L) == 5);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc41_var += lc43_step;
  }
  lua_settop(L,lc44);
  assert(lua_gettop(L) == 4);

  /* return sum */
  lua_pushvalue(L,4);
  return 1;
  assert(lua_gettop(L) == 4);
}


/* name: M.dot_sparse_sparse
 * function(x, y) */
int stuartml_linalg_BLAS_dot_sparse_sparse (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local nnzx = #x.indices */
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  const double lc45 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc45);
  assert(lua_gettop(L) == 3);

  /* local nnzy = #y.indices */
  lua_pushliteral(L,"indices");
  lua_gettable(L,2);
  const double lc46 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc46);
  assert(lua_gettop(L) == 4);

  /* local kx = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 5);

  /* local ky = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 6);

  /* local sum = 0.0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 7);

  /* while kx < nnzx and ky < nnzy do */
  enum { lc47 = 7 };
  while (1) {
    const int lc48 = lua_lessthan(L,5,3);
    lua_pushboolean(L,lc48);
    if (lua_toboolean(L,-1)) {
      lua_pop(L,1);
      const int lc49 = lua_lessthan(L,6,4);
      lua_pushboolean(L,lc49);
    }
    if (!(lua_toboolean(L,-1))) {
      break;
    }
    lua_pop(L,1);

    /* local ix = x.indices[kx+1] */
    lua_pushliteral(L,"indices");
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lc_add(L,5,-1);
    lua_remove(L,-2);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    assert(lua_gettop(L) == 8);

    /* while ky < nnzy and y.indices[ky+1] < ix do */
    enum { lc50 = 8 };
    while (1) {
      const int lc51 = lua_lessthan(L,6,4);
      lua_pushboolean(L,lc51);
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lua_pushliteral(L,"indices");
        lua_gettable(L,2);
        lua_pushnumber(L,1);
        lc_add(L,6,-1);
        lua_remove(L,-2);
        lua_gettable(L,-2);
        lua_remove(L,-2);
        const int lc52 = lua_lessthan(L,-1,8);
        lua_pop(L,1);
        lua_pushboolean(L,lc52);
      }
      if (!(lua_toboolean(L,-1))) {
        break;
      }
      lua_pop(L,1);

      /* ky = ky + 1 */
      lua_pushnumber(L,1);
      lc_add(L,6,-1);
      lua_remove(L,-2);
      lua_replace(L,6);
      assert(lua_gettop(L) == 8);
    }
    lua_settop(L,lc50);
    assert(lua_gettop(L) == 8);

    /* if ky < nnzy and y.indices[ky+1] == ix then */
    enum { lc53 = 8 };
    const int lc54 = lua_lessthan(L,6,4);
    lua_pushboolean(L,lc54);
    if (lua_toboolean(L,-1)) {
      lua_pop(L,1);
      lua_pushliteral(L,"indices");
      lua_gettable(L,2);
      lua_pushnumber(L,1);
      lc_add(L,6,-1);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushvalue(L,8);
      const int lc55 = lua_equal(L,-2,-1);
      lua_pop(L,2);
      lua_pushboolean(L,lc55);
    }
    const int lc56 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc56) {

      /* sum = sum + x.values[kx+1] * y.values[ky+1] */
      lua_pushliteral(L,"values");
      lua_gettable(L,1);
      lua_pushnumber(L,1);
      lc_add(L,5,-1);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"values");
      lua_gettable(L,2);
      lua_pushnumber(L,1);
      lc_add(L,6,-1);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lc_mul(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lc_add(L,7,-1);
      lua_remove(L,-2);
      lua_replace(L,7);
      assert(lua_gettop(L) == 8);

      /* ky = ky + 1 */
      lua_pushnumber(L,1);
      lc_add(L,6,-1);
      lua_remove(L,-2);
      lua_replace(L,6);
      assert(lua_gettop(L) == 8);
    }
    lua_settop(L,lc53);
    assert(lua_gettop(L) == 8);

    /* kx = kx + 1 */
    lua_pushnumber(L,1);
    lc_add(L,5,-1);
    lua_remove(L,-2);
    lua_replace(L,5);
    assert(lua_gettop(L) == 8);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc47);
  assert(lua_gettop(L) == 7);

  /* return sum */
  lua_pushvalue(L,7);
  return 1;
  assert(lua_gettop(L) == 7);
}


/* name: M.scal
 * function(a, x) */
int stuartml_linalg_BLAS_scal (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* for i=1,#x.values do */
  lua_pushnumber(L,1);
  lua_pushliteral(L,"values");
  lua_gettable(L,2);
  const double lc60 = lua_objlen(L,-1);
  lua_pop(L,1);
  lua_pushnumber(L,lc60);
  if (!((lua_isnumber(L,-2) && lua_isnumber(L,-1)))) {
    luaL_error(L,"'for' limit must be a number");
  }
  double lc57_var = lua_tonumber(L,-2);
  const double lc58_limit = lua_tonumber(L,-1);
  const double lc59_step = 1;
  lua_pop(L,2);
  enum { lc61 = 2 };
  while ((((lc59_step > 0) && (lc57_var <= lc58_limit)) || ((lc59_step <= 0) && (lc57_var >= lc58_limit)))) {

    /* internal: local i at index 3 */
    lua_pushnumber(L,lc57_var);

    /* x.values[i] = a * x.values[i] */
    lua_pushliteral(L,"values");
    lua_gettable(L,2);
    lua_pushvalue(L,3);
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lc_mul(L,1,-1);
    lua_remove(L,-2);
    lua_pushliteral(L,"values");
    lua_gettable(L,2);
    lua_insert(L,-2);
    lua_pushvalue(L,3);
    lua_insert(L,-2);
    lua_settable(L,-3);
    lua_pop(L,1);
    assert(lua_gettop(L) == 3);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
    lc57_var += lc59_step;
  }
  lua_settop(L,lc61);
  assert(lua_gettop(L) == 2);
  return 0;
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgblas_map[] = {
  { LSTRKEY("axpy")              , LFUNCVAL (stuartml_linalg_BLAS_axpy) },
  { LSTRKEY("axpy_sparse_dense") , LFUNCVAL (stuartml_linalg_BLAS_axpy_sparse_dense) },
  { LSTRKEY("dot")               , LFUNCVAL (stuartml_linalg_BLAS_dot) },
  { LSTRKEY("dot_sparse_dense")  , LFUNCVAL (stuartml_linalg_BLAS_dot_sparse_dense) },
  { LSTRKEY("dot_sparse_sparse") , LFUNCVAL (stuartml_linalg_BLAS_dot_sparse_sparse) },
  { LSTRKEY("scal")              , LFUNCVAL (stuartml_linalg_BLAS_scal) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgblas( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGBLAS, stuartml_linalgblas_map );
  return 1;
#endif
}
