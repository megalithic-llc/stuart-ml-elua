#ifndef __STUARTML_LINALG_MATRIX_H__
#define __STUARTML_LINALG_MATRIX_H__

#include "lua.h"

extern LUA_REG_TYPE stuartml_linalgmat_map[];

int stuartml_linalg_Matrix___eq (lua_State * L);
int stuartml_linalg_Matrix__init (lua_State * L);
int stuartml_linalg_Matrix_multiply (lua_State * L);
int stuartml_linalg_Matrix_toArray (lua_State * L);
int stuartml_linalg_Matrix_toString (lua_State * L);

#endif
