#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"
#include "stuart_class.h"
#include "stuart-ml_linalg_vector.h"

#define MODULE "stuart-ml.linalg.Vectors"


/* function(...) */
int stuartml_linalg_Vectors_dense (lua_State * L) {
  enum { lc_nformalargs = 0 };
  const int lc_nactualargs = lua_gettop(L);
  const int lc_nextra = (lc_nactualargs - lc_nformalargs);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) - lc_nextra == 1);

  /* local DenseVector = require 'stuart-ml.linalg.DenseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) - lc_nextra == 2);

  /* if moses.isTable(...) then */
  enum { lc1 = 2 };
  lua_pushliteral(L,"isTable");
  lua_gettable(L,(1 + lc_nextra));
  const int lc2 = lua_gettop(L);
  {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
  lua_call(L,(lua_gettop(L) - lc2),1);
  const int lc3 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc3) {

    /* return DenseVector.new(...) */
    const int lc4 = lua_gettop(L);
    lua_pushliteral(L,"new");
    lua_gettable(L,(2 + lc_nextra));
    const int lc5 = lua_gettop(L);
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    lua_call(L,(lua_gettop(L) - lc5),LUA_MULTRET);
    return (lua_gettop(L) - lc4);
    assert(lua_gettop(L) - lc_nextra == 2);
  }
  else {

    /* else
     * return DenseVector.new({...}) */
    const int lc6 = lua_gettop(L);
    lua_pushliteral(L,"new");
    lua_gettable(L,(2 + lc_nextra));
    lua_createtable(L,1,0);
    const int lc7 = lua_gettop(L);
    {int i; for (i=lc_nformalargs+1; i<=lc_nactualargs; i++) { lua_pushvalue(L, i); }}
    while ((lua_gettop(L) > lc7)) {
      lua_rawseti(L,lc7,(0 + (lua_gettop(L) - lc7)));
    }
    lua_call(L,1,LUA_MULTRET);
    return (lua_gettop(L) - lc6);
    assert(lua_gettop(L) - lc_nextra == 2);
  }
  lua_settop(L,(lc1 + lc_nextra));
  assert(lua_gettop(L) - lc_nextra == 2);
  return 0;
}


/* name: M.norm
 * function(vector, p) */
int stuartml_linalg_Vectors_norm (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* assert(p >= 1.0, 'To compute the p-norm of the vector, we require that you specify a p>=1. You specified ' .. p) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnumber(L,1);
  const int lc8 = lc_le(L,-1,2);
  lua_pop(L,1);
  lua_pushboolean(L,lc8);
  lua_pushliteral(L,"To compute the p-norm of the vector, we require that you specify a p>=1. You specified ");
  lua_pushvalue(L,2);
  lua_concat(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 2);

  /* local values = vector.values */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  assert(lua_gettop(L) == 3);

  /* local size = #values */
  const double lc9 = lua_objlen(L,3);
  lua_pushnumber(L,lc9);
  assert(lua_gettop(L) == 4);

  /* if p == 1 then */
  enum { lc10 = 4 };
  lua_pushnumber(L,1);
  const int lc11 = lua_equal(L,2,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc11);
  const int lc12 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc12) {

    /* local sum = 0.0 */
    lua_pushnumber(L,0);
    assert(lua_gettop(L) == 5);

    /* for i=1,size do */
    lua_pushnumber(L,1);
    if (!((lua_isnumber(L,-1) && lua_isnumber(L,4)))) {
      luaL_error(L,"'for' limit must be a number");
    }
    double lc13_var = lua_tonumber(L,-1);
    const double lc14_limit = lua_tonumber(L,4);
    const double lc15_step = 1;
    lua_pop(L,1);
    enum { lc16 = 5 };
    while ((((lc15_step > 0) && (lc13_var <= lc14_limit)) || ((lc15_step <= 0) && (lc13_var >= lc14_limit)))) {

      /* internal: local i at index 6 */
      lua_pushnumber(L,lc13_var);

      /* sum = sum + math.abs(values[i]) */
      lua_getfield(L,LUA_ENVIRONINDEX,"math");
      lua_pushliteral(L,"abs");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushvalue(L,6);
      lua_gettable(L,3);
      lua_call(L,1,1);
      lc_add(L,5,-1);
      lua_remove(L,-2);
      lua_replace(L,5);
      assert(lua_gettop(L) == 6);

      /* internal: stack cleanup on scope exit */
      lua_pop(L,1);
      lc13_var += lc15_step;
    }
    lua_settop(L,lc16);
    assert(lua_gettop(L) == 5);

    /* return sum */
    lua_pushvalue(L,5);
    return 1;
    assert(lua_gettop(L) == 5);
  }
  else {

    /* elseif p == 2 then */
    enum { lc17 = 4 };
    lua_pushnumber(L,2);
    const int lc18 = lua_equal(L,2,-1);
    lua_pop(L,1);
    lua_pushboolean(L,lc18);
    const int lc19 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc19) {

      /* local sum = 0.0 */
      lua_pushnumber(L,0);
      assert(lua_gettop(L) == 5);

      /* for i=1,size do */
      lua_pushnumber(L,1);
      if (!((lua_isnumber(L,-1) && lua_isnumber(L,4)))) {
        luaL_error(L,"'for' limit must be a number");
      }
      double lc20_var = lua_tonumber(L,-1);
      const double lc21_limit = lua_tonumber(L,4);
      const double lc22_step = 1;
      lua_pop(L,1);
      enum { lc23 = 5 };
      while ((((lc22_step > 0) && (lc20_var <= lc21_limit)) || ((lc22_step <= 0) && (lc20_var >= lc21_limit)))) {

        /* internal: local i at index 6 */
        lua_pushnumber(L,lc20_var);

        /* sum = sum + values[i] * values[i] */
        lua_pushvalue(L,6);
        lua_gettable(L,3);
        lua_pushvalue(L,6);
        lua_gettable(L,3);
        lc_mul(L,-2,-1);
        lua_remove(L,-2);
        lua_remove(L,-2);
        lc_add(L,5,-1);
        lua_remove(L,-2);
        lua_replace(L,5);
        assert(lua_gettop(L) == 6);

        /* internal: stack cleanup on scope exit */
        lua_pop(L,1);
        lc20_var += lc22_step;
      }
      lua_settop(L,lc23);
      assert(lua_gettop(L) == 5);

      /* return math.sqrt(sum) */
      const int lc24 = lua_gettop(L);
      lua_getfield(L,LUA_ENVIRONINDEX,"math");
      lua_pushliteral(L,"sqrt");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushvalue(L,5);
      lua_call(L,1,LUA_MULTRET);
      return (lua_gettop(L) - lc24);
      assert(lua_gettop(L) == 5);
    }
    else {

      /* elseif p == math.huge then */
      enum { lc25 = 4 };
      lua_getfield(L,LUA_ENVIRONINDEX,"math");
      lua_pushliteral(L,"huge");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      const int lc26 = lua_equal(L,2,-1);
      lua_pop(L,1);
      lua_pushboolean(L,lc26);
      const int lc27 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc27) {

        /* local max = 0.0 */
        lua_pushnumber(L,0);
        assert(lua_gettop(L) == 5);

        /* for i=1,size do */
        lua_pushnumber(L,1);
        if (!((lua_isnumber(L,-1) && lua_isnumber(L,4)))) {
          luaL_error(L,"'for' limit must be a number");
        }
        double lc28_var = lua_tonumber(L,-1);
        const double lc29_limit = lua_tonumber(L,4);
        const double lc30_step = 1;
        lua_pop(L,1);
        enum { lc31 = 5 };
        while ((((lc30_step > 0) && (lc28_var <= lc29_limit)) || ((lc30_step <= 0) && (lc28_var >= lc29_limit)))) {

          /* internal: local i at index 6 */
          lua_pushnumber(L,lc28_var);

          /* local value = math.abs(values[i]) */
          lua_getfield(L,LUA_ENVIRONINDEX,"math");
          lua_pushliteral(L,"abs");
          lua_gettable(L,-2);
          lua_remove(L,-2);
          lua_pushvalue(L,6);
          lua_gettable(L,3);
          lua_call(L,1,1);
          assert(lua_gettop(L) == 7);

          /* if value > max then */
          enum { lc32 = 7 };
          const int lc33 = lua_lessthan(L,5,7);
          lua_pushboolean(L,lc33);
          const int lc34 = lua_toboolean(L,-1);
          lua_pop(L,1);
          if (lc34) {

            /* max = value */
            lua_pushvalue(L,7);
            lua_replace(L,5);
            assert(lua_gettop(L) == 7);
          }
          lua_settop(L,lc32);
          assert(lua_gettop(L) == 7);

          /* internal: stack cleanup on scope exit */
          lua_pop(L,2);
          lc28_var += lc30_step;
        }
        lua_settop(L,lc31);
        assert(lua_gettop(L) == 5);

        /* return max */
        lua_pushvalue(L,5);
        return 1;
        assert(lua_gettop(L) == 5);
      }
      else {

        /* else
         * local sum = 0.0 */
        lua_pushnumber(L,0);
        assert(lua_gettop(L) == 5);

        /* for i=1,size do */
        lua_pushnumber(L,1);
        if (!((lua_isnumber(L,-1) && lua_isnumber(L,4)))) {
          luaL_error(L,"'for' limit must be a number");
        }
        double lc35_var = lua_tonumber(L,-1);
        const double lc36_limit = lua_tonumber(L,4);
        const double lc37_step = 1;
        lua_pop(L,1);
        enum { lc38 = 5 };
        while ((((lc37_step > 0) && (lc35_var <= lc36_limit)) || ((lc37_step <= 0) && (lc35_var >= lc36_limit)))) {

          /* internal: local i at index 6 */
          lua_pushnumber(L,lc35_var);

          /* sum = sum + math.pow(math.abs(values[i]), p) */
          lua_getfield(L,LUA_ENVIRONINDEX,"math");
          lua_pushliteral(L,"pow");
          lua_gettable(L,-2);
          lua_remove(L,-2);
          lua_getfield(L,LUA_ENVIRONINDEX,"math");
          lua_pushliteral(L,"abs");
          lua_gettable(L,-2);
          lua_remove(L,-2);
          lua_pushvalue(L,6);
          lua_gettable(L,3);
          lua_call(L,1,1);
          lua_pushvalue(L,2);
          lua_call(L,2,1);
          lc_add(L,5,-1);
          lua_remove(L,-2);
          lua_replace(L,5);
          assert(lua_gettop(L) == 6);

          /* internal: stack cleanup on scope exit */
          lua_pop(L,1);
          lc35_var += lc37_step;
        }
        lua_settop(L,lc38);
        assert(lua_gettop(L) == 5);

        /* return math.pow(sum, 1.0 / p) */
        const int lc39 = lua_gettop(L);
        lua_getfield(L,LUA_ENVIRONINDEX,"math");
        lua_pushliteral(L,"pow");
        lua_gettable(L,-2);
        lua_remove(L,-2);
        lua_pushvalue(L,5);
        lua_pushnumber(L,1);
        lc_div(L,-1,2);
        lua_remove(L,-2);
        lua_call(L,2,LUA_MULTRET);
        return (lua_gettop(L) - lc39);
        assert(lua_gettop(L) == 5);
      }
      lua_settop(L,lc25);
    }
    lua_settop(L,lc17);
  }
  lua_settop(L,lc10);
  assert(lua_gettop(L) == 4);
  return 0;
}


/* function(a,b) */
int stuartml_linalg_Vectors_sparse_1 (lua_State * L) {
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* if moses.isTable(a) and moses.isTable(b) then */
  enum { lc44 = 2 };
  lc_getupvalue(L,lua_upvalueindex(1),0,1);
  lua_pushliteral(L,"isTable");
  lua_gettable(L,-2);
  lua_remove(L,-2);
  lua_pushvalue(L,1);
  lua_call(L,1,1);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lc_getupvalue(L,lua_upvalueindex(1),0,1);
    lua_pushliteral(L,"isTable");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,2);
    lua_call(L,1,1);
  }
  const int lc45 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc45) {

    /* return a[1] < b[1] */
    lua_pushnumber(L,1);
    lua_gettable(L,1);
    lua_pushnumber(L,1);
    lua_gettable(L,2);
    const int lc46 = lua_lessthan(L,-2,-1);
    lua_pop(L,2);
    lua_pushboolean(L,lc46);
    return 1;
    assert(lua_gettop(L) == 2);
  }
  lua_settop(L,lc44);
  assert(lua_gettop(L) == 2);
  return 0;
}


/* name: M.sparse
 * function(size, arg2, arg3) */
int stuartml_linalg_Vectors_sparse (lua_State * L) {
  lua_checkstack(L,20);
  enum { lc_nformalargs = 3 };
  lua_settop(L,3);

  /* local moses = require 'moses' */
  lc_newclosuretable(L,lua_upvalueindex(1));
  enum { lc40 = 4 };
  assert((lua_gettop(L) == lc40));
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  lua_rawseti(L,lc40,1);
  assert(lua_gettop(L) == 4);

  /* local SparseVector = require 'stuart-ml.linalg.SparseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* if arg3 == nil then */
  enum { lc41 = 5 };
  lua_pushnil(L);
  const int lc42 = lua_equal(L,3,-1);
  lua_pop(L,1);
  lua_pushboolean(L,lc42);
  const int lc43 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc43) {

    /* -- arg2 is elements
     * local elements = moses.sort(arg2, function(a,b)
     *       if moses.isTable(a) and moses.isTable(b) then return a[1] < b[1] end
     *     end) */
    lc_getupvalue(L,lc40,0,1);
    lua_pushliteral(L,"sort");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    lua_pushvalue(L,2);
    lua_pushvalue(L,lc40);
    lua_pushcclosure(L,stuartml_linalg_Vectors_sparse_1,1);
    lua_call(L,2,1);
    assert(lua_gettop(L) == 6);

    /* local unpack = table.unpack or unpack */
    lua_getfield(L,LUA_ENVIRONINDEX,"table");
    lua_pushliteral(L,"unpack");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    if (!(lua_toboolean(L,-1))) {
      lua_pop(L,1);
      lua_getfield(L,LUA_ENVIRONINDEX,"unpack");
    }
    assert(lua_gettop(L) == 7);

    /* local unzip = require 'stuart-ml.util'.unzip */
    lua_getfield(L,LUA_ENVIRONINDEX,"require");
    lua_pushliteral(L,"stuart-ml.util");
    lua_call(L,1,1);
    lua_pushliteral(L,"unzip");
    lua_gettable(L,-2);
    lua_remove(L,-2);
    assert(lua_gettop(L) == 8);

    /* local indices, values = unpack(unzip(elements)) */
    lua_pushvalue(L,7);
    const int lc48 = lua_gettop(L);
    lua_pushvalue(L,8);
    lua_pushvalue(L,6);
    lua_call(L,1,LUA_MULTRET);
    lua_call(L,(lua_gettop(L) - lc48),2);
    assert(lua_gettop(L) == 10);

    /* --    var prev = -1
     * --    indices.foreach { i =>
     * --      require(prev < i, s"Found duplicate indices: $i.")
     * --      prev = i
     * --    }
     * --    require(prev < size, s"You may not write an element to index $prev because the declared " +
     * --      s"size of your vector is $size")
     * return SparseVector.new(size, indices, values) */
    const int lc49 = lua_gettop(L);
    lua_pushliteral(L,"new");
    lua_gettable(L,5);
    lua_pushvalue(L,1);
    lua_pushvalue(L,9);
    lua_pushvalue(L,10);
    lua_call(L,3,LUA_MULTRET);
    return (lua_gettop(L) - lc49);
    assert(lua_gettop(L) == 10);
  }
  else {

    /* else
     * -- arg2 is indices, arg3 is values
     * return SparseVector.new(size, arg2, arg3) */
    const int lc50 = lua_gettop(L);
    lua_pushliteral(L,"new");
    lua_gettable(L,5);
    lua_pushvalue(L,1);
    lua_pushvalue(L,2);
    lua_pushvalue(L,3);
    lua_call(L,3,LUA_MULTRET);
    return (lua_gettop(L) - lc50);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc41);
  assert(lua_gettop(L) == 5);
  return 0;
}


/* name: M.sqdist
 * function(v1, v2) */
int stuartml_linalg_Vectors_sqdist (lua_State * L) {
  lua_checkstack(L,20);
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* assert(v1:size() == v2:size(), 'Vector dimensions do not match: Dim(v1)=' .. v1:size()
   *     .. ' and Dim(v2)=' .. v2:size()) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,2);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  const int lc51 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc51);
  lua_pushliteral(L,"Vector dimensions do not match: Dim(v1)=");
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushliteral(L," and Dim(v2)=");
  lua_pushvalue(L,2);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_call(L,2,0);
  assert(lua_gettop(L) == 2);

  /* local class = require 'stuart.class' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 3);

  /* local SparseVector = require 'stuart-ml.linalg.SparseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* local M = require 'stuart-ml.linalg.Vectors' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 5);

  /* if class.istype(v1,SparseVector) and class.istype(v2,SparseVector) then */
  enum { lc52 = 5 };
  lua_pushliteral(L,"istype");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,4);
  lua_call(L,2,1);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"istype");
    lua_gettable(L,3);
    lua_pushvalue(L,2);
    lua_pushvalue(L,4);
    lua_call(L,2,1);
  }
  const int lc53 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc53) {

    /* return M.sqdist_sparse_sparse(v1, v2) */
    const int lc54 = lua_gettop(L);
    lua_pushliteral(L,"sqdist_sparse_sparse");
    lua_gettable(L,5);
    lua_pushvalue(L,1);
    lua_pushvalue(L,2);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc54);
    assert(lua_gettop(L) == 5);
  }
  lua_settop(L,lc52);
  assert(lua_gettop(L) == 5);

  /* local DenseVector = require 'stuart-ml.linalg.DenseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* if class.istype(v1,SparseVector) and class.istype(v2,DenseVector) then */
  enum { lc55 = 6 };
  lua_pushliteral(L,"istype");
  lua_gettable(L,3);
  lua_pushvalue(L,1);
  lua_pushvalue(L,4);
  lua_call(L,2,1);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushliteral(L,"istype");
    lua_gettable(L,3);
    lua_pushvalue(L,2);
    lua_pushvalue(L,6);
    lua_call(L,2,1);
  }
  const int lc56 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc56) {

    /* return M.sqdist_sparse_dense(v1, v2) */
    const int lc57 = lua_gettop(L);
    lua_pushliteral(L,"sqdist_sparse_dense");
    lua_gettable(L,5);
    lua_pushvalue(L,1);
    lua_pushvalue(L,2);
    lua_call(L,2,LUA_MULTRET);
    return (lua_gettop(L) - lc57);
    assert(lua_gettop(L) == 6);
  }
  else {

    /* elseif class.istype(v1,DenseVector) and class.istype(v2,SparseVector) then */
    enum { lc58 = 6 };
    lua_pushliteral(L,"istype");
    lua_gettable(L,3);
    lua_pushvalue(L,1);
    lua_pushvalue(L,6);
    lua_call(L,2,1);
    if (lua_toboolean(L,-1)) {
      lua_pop(L,1);
      lua_pushliteral(L,"istype");
      lua_gettable(L,3);
      lua_pushvalue(L,2);
      lua_pushvalue(L,4);
      lua_call(L,2,1);
    }
    const int lc59 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc59) {

      /* return M.sqdist_sparse_dense(v2, v1) */
      const int lc60 = lua_gettop(L);
      lua_pushliteral(L,"sqdist_sparse_dense");
      lua_gettable(L,5);
      lua_pushvalue(L,2);
      lua_pushvalue(L,1);
      lua_call(L,2,LUA_MULTRET);
      return (lua_gettop(L) - lc60);
      assert(lua_gettop(L) == 6);
    }
    else {

      /* elseif class.istype(v1,DenseVector) and class.istype(v2,DenseVector) then */
      enum { lc61 = 6 };
      lua_pushliteral(L,"istype");
      lua_gettable(L,3);
      lua_pushvalue(L,1);
      lua_pushvalue(L,6);
      lua_call(L,2,1);
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lua_pushliteral(L,"istype");
        lua_gettable(L,3);
        lua_pushvalue(L,2);
        lua_pushvalue(L,6);
        lua_call(L,2,1);
      }
      const int lc62 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc62) {

        /* local kv = 0 */
        lua_pushnumber(L,0);
        assert(lua_gettop(L) == 7);

        /* local sz = #v1 */
        const double lc63 = lua_objlen(L,1);
        lua_pushnumber(L,lc63);
        assert(lua_gettop(L) == 8);

        /* local squaredDistance = 0.0 */
        lua_pushnumber(L,0);
        assert(lua_gettop(L) == 9);

        /* while kv < sz do */
        enum { lc64 = 9 };
        while (1) {
          const int lc65 = lua_lessthan(L,7,8);
          lua_pushboolean(L,lc65);
          if (!(lua_toboolean(L,-1))) {
            break;
          }
          lua_pop(L,1);

          /* local score = v1[kv+1] - v2[kv+1] */
          lua_pushnumber(L,1);
          lc_add(L,7,-1);
          lua_remove(L,-2);
          lua_gettable(L,1);
          lua_pushnumber(L,1);
          lc_add(L,7,-1);
          lua_remove(L,-2);
          lua_gettable(L,2);
          lc_sub(L,-2,-1);
          lua_remove(L,-2);
          lua_remove(L,-2);
          assert(lua_gettop(L) == 10);

          /* squaredDistance = squaredDistance + score * score */
          lc_mul(L,10,10);
          lc_add(L,9,-1);
          lua_remove(L,-2);
          lua_replace(L,9);
          assert(lua_gettop(L) == 10);

          /* kv = kv + 1 */
          lua_pushnumber(L,1);
          lc_add(L,7,-1);
          lua_remove(L,-2);
          lua_replace(L,7);
          assert(lua_gettop(L) == 10);

          /* internal: stack cleanup on scope exit */
          lua_pop(L,1);
        }
        lua_settop(L,lc64);
        assert(lua_gettop(L) == 9);

        /* return squaredDistance */
        lua_pushvalue(L,9);
        return 1;
        assert(lua_gettop(L) == 9);
      }
      lua_settop(L,lc61);
    }
    lua_settop(L,lc58);
  }
  lua_settop(L,lc55);
  assert(lua_gettop(L) == 6);

  /* error('Unsupported vector type ' .. v1 .. ' and ' .. v2) */
  lua_getfield(L,LUA_ENVIRONINDEX,"error");
  lua_pushliteral(L,"Unsupported vector type ");
  lua_pushvalue(L,1);
  lua_pushliteral(L," and ");
  lua_pushvalue(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_concat(L,2);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 6);
  return 0;
}


/* name: M.sqdist_sparse_sparse
 * function(v1, v2) */
int stuartml_linalg_Vectors_sqdist_sparse_sparse (lua_State * L) {
  lua_checkstack(L,22);
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local squaredDistance = 0.0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 3);

  /* local v1Values = v1.values */
  lua_pushliteral(L,"values");
  lua_gettable(L,1);
  assert(lua_gettop(L) == 4);

  /* local v1Indices = v1.indices */
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  assert(lua_gettop(L) == 5);

  /* local v2Values = v2.values */
  lua_pushliteral(L,"values");
  lua_gettable(L,2);
  assert(lua_gettop(L) == 6);

  /* local v2Indices = v2.indices */
  lua_pushliteral(L,"indices");
  lua_gettable(L,2);
  assert(lua_gettop(L) == 7);

  /* local nnzv1 = #v1Indices */
  const double lc66 = lua_objlen(L,5);
  lua_pushnumber(L,lc66);
  assert(lua_gettop(L) == 8);

  /* local nnzv2 = #v2Indices */
  const double lc67 = lua_objlen(L,7);
  lua_pushnumber(L,lc67);
  assert(lua_gettop(L) == 9);

  /* local kv1 = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 10);

  /* local kv2 = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 11);

  /* while kv1 < nnzv1 or kv2 < nnzv2 do */
  enum { lc68 = 11 };
  while (1) {
    const int lc69 = lua_lessthan(L,10,8);
    lua_pushboolean(L,lc69);
    if (!(lua_toboolean(L,-1))) {
      lua_pop(L,1);
      const int lc70 = lua_lessthan(L,11,9);
      lua_pushboolean(L,lc70);
    }
    if (!(lua_toboolean(L,-1))) {
      break;
    }
    lua_pop(L,1);

    /* local score = 0.0 */
    lua_pushnumber(L,0);
    assert(lua_gettop(L) == 12);

    /* if kv2 >= nnzv2 or (kv1 < nnzv1 and v1Indices[kv1+1] < v2Indices[kv2+1]) then */
    enum { lc71 = 12 };
    lua_pushboolean(L,lc_le(L,9,11));
    if (!(lua_toboolean(L,-1))) {
      lua_pop(L,1);
      const int lc72 = lua_lessthan(L,10,8);
      lua_pushboolean(L,lc72);
      if (lua_toboolean(L,-1)) {
        lua_pop(L,1);
        lua_pushnumber(L,1);
        lc_add(L,10,-1);
        lua_remove(L,-2);
        lua_gettable(L,5);
        lua_pushnumber(L,1);
        lc_add(L,11,-1);
        lua_remove(L,-2);
        lua_gettable(L,7);
        const int lc73 = lua_lessthan(L,-2,-1);
        lua_pop(L,2);
        lua_pushboolean(L,lc73);
      }
    }
    const int lc74 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc74) {

      /* score = v1Values[kv1+1] */
      lua_pushnumber(L,1);
      lc_add(L,10,-1);
      lua_remove(L,-2);
      lua_gettable(L,4);
      lua_replace(L,12);
      assert(lua_gettop(L) == 12);

      /* kv1 = kv1 + 1 */
      lua_pushnumber(L,1);
      lc_add(L,10,-1);
      lua_remove(L,-2);
      lua_replace(L,10);
      assert(lua_gettop(L) == 12);
    }
    else {

      /* elseif kv1 >= nnzv1 or (kv2 < nnzv2 and v2Indices[kv2+1] < v1Indices[kv1+1]) then */
      enum { lc75 = 12 };
      lua_pushboolean(L,lc_le(L,8,10));
      if (!(lua_toboolean(L,-1))) {
        lua_pop(L,1);
        const int lc76 = lua_lessthan(L,11,9);
        lua_pushboolean(L,lc76);
        if (lua_toboolean(L,-1)) {
          lua_pop(L,1);
          lua_pushnumber(L,1);
          lc_add(L,11,-1);
          lua_remove(L,-2);
          lua_gettable(L,7);
          lua_pushnumber(L,1);
          lc_add(L,10,-1);
          lua_remove(L,-2);
          lua_gettable(L,5);
          const int lc77 = lua_lessthan(L,-2,-1);
          lua_pop(L,2);
          lua_pushboolean(L,lc77);
        }
      }
      const int lc78 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc78) {

        /* score = v2Values[kv2+1] */
        lua_pushnumber(L,1);
        lc_add(L,11,-1);
        lua_remove(L,-2);
        lua_gettable(L,6);
        lua_replace(L,12);
        assert(lua_gettop(L) == 12);

        /* kv2 = kv2 + 1 */
        lua_pushnumber(L,1);
        lc_add(L,11,-1);
        lua_remove(L,-2);
        lua_replace(L,11);
        assert(lua_gettop(L) == 12);
      }
      else {

        /* else
         * score = v1Values[kv1+1] - v2Values[kv2+1] */
        lua_pushnumber(L,1);
        lc_add(L,10,-1);
        lua_remove(L,-2);
        lua_gettable(L,4);
        lua_pushnumber(L,1);
        lc_add(L,11,-1);
        lua_remove(L,-2);
        lua_gettable(L,6);
        lc_sub(L,-2,-1);
        lua_remove(L,-2);
        lua_remove(L,-2);
        lua_replace(L,12);
        assert(lua_gettop(L) == 12);

        /* kv1 = kv1 + 1 */
        lua_pushnumber(L,1);
        lc_add(L,10,-1);
        lua_remove(L,-2);
        lua_replace(L,10);
        assert(lua_gettop(L) == 12);

        /* kv2 = kv2 + 1 */
        lua_pushnumber(L,1);
        lc_add(L,11,-1);
        lua_remove(L,-2);
        lua_replace(L,11);
        assert(lua_gettop(L) == 12);
      }
      lua_settop(L,lc75);
    }
    lua_settop(L,lc71);
    assert(lua_gettop(L) == 12);

    /* squaredDistance = squaredDistance + score * score */
    lc_mul(L,12,12);
    lc_add(L,3,-1);
    lua_remove(L,-2);
    lua_replace(L,3);
    assert(lua_gettop(L) == 12);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc68);
  assert(lua_gettop(L) == 11);

  /* return squaredDistance */
  lua_pushvalue(L,3);
  return 1;
  assert(lua_gettop(L) == 11);
}


/* name: M.sqdist_sparse_dense
 * function(v1, v2) */
int stuartml_linalg_Vectors_sqdist_sparse_dense (lua_State * L) {
  lua_checkstack(L,20);
  enum { lc_nformalargs = 2 };
  lua_settop(L,2);

  /* local kv1 = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 3);

  /* local kv2 = 0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 4);

  /* local indices = v1.indices */
  lua_pushliteral(L,"indices");
  lua_gettable(L,1);
  assert(lua_gettop(L) == 5);

  /* local squaredDistance = 0.0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 6);

  /* local nnzv1 = #indices */
  const double lc79 = lua_objlen(L,5);
  lua_pushnumber(L,lc79);
  assert(lua_gettop(L) == 7);

  /* local nnzv2 = v2:size() */
  lua_pushvalue(L,2);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 8);

  /* local iv1 = -1 */
  lua_pushnumber(L,-1);
  assert(lua_gettop(L) == 9);

  /* if nnzv1 > 0 then */
  enum { lc80 = 9 };
  lua_pushnumber(L,0);
  const int lc81 = lua_lessthan(L,-1,7);
  lua_pop(L,1);
  lua_pushboolean(L,lc81);
  const int lc82 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc82) {

    /* iv1 = indices[kv1+1] */
    lua_pushnumber(L,1);
    lc_add(L,3,-1);
    lua_remove(L,-2);
    lua_gettable(L,5);
    lua_replace(L,9);
    assert(lua_gettop(L) == 9);
  }
  lua_settop(L,lc80);
  assert(lua_gettop(L) == 9);

  /* while kv2 < nnzv2 do */
  enum { lc83 = 9 };
  while (1) {
    const int lc84 = lua_lessthan(L,4,8);
    lua_pushboolean(L,lc84);
    if (!(lua_toboolean(L,-1))) {
      break;
    }
    lua_pop(L,1);

    /* local score = 0.0 */
    lua_pushnumber(L,0);
    assert(lua_gettop(L) == 10);

    /* if kv2 ~= iv1 then */
    enum { lc85 = 10 };
    const int lc86 = lua_equal(L,4,9);
    lua_pushboolean(L,lc86);
    lua_pushboolean(L,!(lua_toboolean(L,-1)));
    lua_remove(L,-2);
    const int lc87 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc87) {

      /* score = v2[kv2+1] */
      lua_pushnumber(L,1);
      lc_add(L,4,-1);
      lua_remove(L,-2);
      lua_gettable(L,2);
      lua_replace(L,10);
      assert(lua_gettop(L) == 10);
    }
    else {

      /* else
       * score = v1.values[kv1+1] - v2[kv2+1] */
      lua_pushliteral(L,"values");
      lua_gettable(L,1);
      lua_pushnumber(L,1);
      lc_add(L,3,-1);
      lua_remove(L,-2);
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushnumber(L,1);
      lc_add(L,4,-1);
      lua_remove(L,-2);
      lua_gettable(L,2);
      lc_sub(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_replace(L,10);
      assert(lua_gettop(L) == 10);

      /* if kv1 < nnzv1 - 1 then */
      enum { lc88 = 10 };
      lua_pushnumber(L,1);
      lc_sub(L,7,-1);
      lua_remove(L,-2);
      const int lc89 = lua_lessthan(L,3,-1);
      lua_pop(L,1);
      lua_pushboolean(L,lc89);
      const int lc90 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc90) {

        /* kv1 = kv1 + 1 */
        lua_pushnumber(L,1);
        lc_add(L,3,-1);
        lua_remove(L,-2);
        lua_replace(L,3);
        assert(lua_gettop(L) == 10);

        /* iv1 = indices[kv1+1] */
        lua_pushnumber(L,1);
        lc_add(L,3,-1);
        lua_remove(L,-2);
        lua_gettable(L,5);
        lua_replace(L,9);
        assert(lua_gettop(L) == 10);
      }
      lua_settop(L,lc88);
      assert(lua_gettop(L) == 10);
    }
    lua_settop(L,lc85);
    assert(lua_gettop(L) == 10);

    /* squaredDistance = squaredDistance + score * score */
    lc_mul(L,10,10);
    lc_add(L,6,-1);
    lua_remove(L,-2);
    lua_replace(L,6);
    assert(lua_gettop(L) == 10);

    /* kv2 = kv2 + 1 */
    lua_pushnumber(L,1);
    lc_add(L,4,-1);
    lua_remove(L,-2);
    lua_replace(L,4);
    assert(lua_gettop(L) == 10);

    /* internal: stack cleanup on scope exit */
    lua_pop(L,1);
  }
  lua_settop(L,lc83);
  assert(lua_gettop(L) == 9);

  /* return squaredDistance */
  lua_pushvalue(L,6);
  return 1;
  assert(lua_gettop(L) == 9);
}


/* name: M.zeros
 * function(size) */
int stuartml_linalg_Vectors_zeros (lua_State * L) {
  enum { lc_nformalargs = 1 };
  lua_settop(L,1);

  /* local moses = require 'moses' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"moses");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 2);

  /* local data = moses.rep(0, size) */
  lua_pushliteral(L,"rep");
  lua_gettable(L,2);
  lua_pushnumber(L,0);
  lua_pushvalue(L,1);
  lua_call(L,2,1);
  assert(lua_gettop(L) == 3);

  /* local DenseVector = require 'stuart-ml.linalg.DenseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.DenseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 4);

  /* return DenseVector.new(data) */
  const int lc91 = lua_gettop(L);
  lua_pushliteral(L,"new");
  lua_gettable(L,4);
  lua_pushvalue(L,3);
  lua_call(L,1,LUA_MULTRET);
  return (lua_gettop(L) - lc91);
  assert(lua_gettop(L) == 4);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_linalgvecs_map[] = {
  { LSTRKEY("dense")               , LFUNCVAL (stuartml_linalg_Vectors_dense) },
  { LSTRKEY("norm")                , LFUNCVAL (stuartml_linalg_Vectors_norm) },
  { LSTRKEY("sparse")              , LFUNCVAL (stuartml_linalg_Vectors_sparse) },
  { LSTRKEY("sqdist")              , LFUNCVAL (stuartml_linalg_Vectors_sqdist) },
  { LSTRKEY("sqdist_sparse_sparse"), LFUNCVAL (stuartml_linalg_Vectors_sqdist_sparse_sparse) },
  { LSTRKEY("sqdist_sparse_dense") , LFUNCVAL (stuartml_linalg_Vectors_sqdist_sparse_dense) },
  { LSTRKEY("zeros")               , LFUNCVAL (stuartml_linalg_Vectors_zeros) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_linalgvecs( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_LINALGVECS, stuartml_linalgvecs_map );
  return 1;
#endif
}
