#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "common.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
#include <assert.h>
#include "lua2c.h"

#define MODULE "stuart-ml.util.MLUtils"


/* name: M.fastSquaredDistance
 * function(v1, norm1, v2, norm2, precision) */
int stuartml_util_fastSquaredDistance (lua_State * L) {
  lua_checkstack(L,27);
  enum { lc_nformalargs = 5 };
  lua_settop(L,5);

  /* precision = precision or 1e-6 */
  lua_pushvalue(L,5);
  if (!(lua_toboolean(L,-1))) {
    lua_pop(L,1);
    lua_pushnumber(L,1e-06);
  }
  lua_replace(L,5);
  assert(lua_gettop(L) == 5);

  /* local n = v1:size() */
  lua_pushvalue(L,1);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 6);

  /* assert(v2:size() == n) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushvalue(L,3);
  lua_pushliteral(L,"size");
  lua_gettable(L,-2);
  lua_insert(L,-2);
  lua_call(L,1,1);
  lua_pushvalue(L,6);
  const int lc1 = lua_equal(L,-2,-1);
  lua_pop(L,2);
  lua_pushboolean(L,lc1);
  lua_call(L,1,0);
  assert(lua_gettop(L) == 6);

  /* assert(norm1 >= 0.0 and norm2 >= 0.0) */
  lua_getfield(L,LUA_ENVIRONINDEX,"assert");
  lua_pushnumber(L,0);
  const int lc2 = lc_le(L,-1,2);
  lua_pop(L,1);
  lua_pushboolean(L,lc2);
  if (lua_toboolean(L,-1)) {
    lua_pop(L,1);
    lua_pushnumber(L,0);
    const int lc3 = lc_le(L,-1,4);
    lua_pop(L,1);
    lua_pushboolean(L,lc3);
  }
  lua_call(L,1,0);
  assert(lua_gettop(L) == 6);

  /* local sumSquaredNorm = norm1 * norm1 + norm2 * norm2 */
  lc_mul(L,2,2);
  lc_mul(L,4,4);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 7);

  /* local normDiff = norm1 - norm2 */
  lc_sub(L,2,4);
  assert(lua_gettop(L) == 8);

  /* local sqDist = 0.0 */
  lua_pushnumber(L,0);
  assert(lua_gettop(L) == 9);

  /* local M = require 'stuart-ml.util.MLUtils' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,MODULE);
  lua_call(L,1,1);
  assert(lua_gettop(L) == 10);

  /* local precisionBound1 = 2.0 * M.EPSILON * sumSquaredNorm / (normDiff * normDiff + M.EPSILON) */
  lua_pushnumber(L,2);
  lua_pushliteral(L,"EPSILON");
  lua_gettable(L,10);
  lc_mul(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_mul(L,-1,7);
  lua_remove(L,-2);
  lc_mul(L,8,8);
  lua_pushliteral(L,"EPSILON");
  lua_gettable(L,10);
  lc_add(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  lc_div(L,-2,-1);
  lua_remove(L,-2);
  lua_remove(L,-2);
  assert(lua_gettop(L) == 11);

  /* local BLAS = require 'stuart-ml.linalg.BLAS' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.BLAS");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 12);

  /* local class = require 'stuart.class' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart.class");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 13);

  /* local Vectors = require 'stuart-ml.linalg.Vectors' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.Vectors");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 14);

  /* local SparseVector = require 'stuart-ml.linalg.SparseVector' */
  lua_getfield(L,LUA_ENVIRONINDEX,"require");
  lua_pushliteral(L,"stuart-ml.linalg.SparseVector");
  lua_call(L,1,1);
  assert(lua_gettop(L) == 15);

  /* if precisionBound1 < precision then */
  enum { lc4 = 15 };
  const int lc5 = lua_lessthan(L,11,5);
  lua_pushboolean(L,lc5);
  const int lc6 = lua_toboolean(L,-1);
  lua_pop(L,1);
  if (lc6) {

    /* sqDist = sumSquaredNorm - 2.0 * BLAS.dot(v1, v2) */
    lua_pushnumber(L,2);
    lua_pushliteral(L,"dot");
    lua_gettable(L,12);
    lua_pushvalue(L,1);
    lua_pushvalue(L,3);
    lua_call(L,2,1);
    lc_mul(L,-2,-1);
    lua_remove(L,-2);
    lua_remove(L,-2);
    lc_sub(L,7,-1);
    lua_remove(L,-2);
    lua_replace(L,9);
    assert(lua_gettop(L) == 15);
  }
  else {

    /* elseif class.istype(v1,SparseVector) or class.istype(v2,SparseVector) then */
    enum { lc7 = 15 };
    lua_pushliteral(L,"istype");
    lua_gettable(L,13);
    lua_pushvalue(L,1);
    lua_pushvalue(L,15);
    lua_call(L,2,1);
    if (!(lua_toboolean(L,-1))) {
      lua_pop(L,1);
      lua_pushliteral(L,"istype");
      lua_gettable(L,13);
      lua_pushvalue(L,3);
      lua_pushvalue(L,15);
      lua_call(L,2,1);
    }
    const int lc8 = lua_toboolean(L,-1);
    lua_pop(L,1);
    if (lc8) {

      /* local dotValue = BLAS.dot(v1, v2) */
      lua_pushliteral(L,"dot");
      lua_gettable(L,12);
      lua_pushvalue(L,1);
      lua_pushvalue(L,3);
      lua_call(L,2,1);
      assert(lua_gettop(L) == 16);

      /* sqDist = math.max(sumSquaredNorm - 2.0 * dotValue, 0.0) */
      lua_getfield(L,LUA_ENVIRONINDEX,"math");
      lua_pushliteral(L,"max");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushnumber(L,2);
      lc_mul(L,-1,16);
      lua_remove(L,-2);
      lc_sub(L,7,-1);
      lua_remove(L,-2);
      lua_pushnumber(L,0);
      lua_call(L,2,1);
      lua_replace(L,9);
      assert(lua_gettop(L) == 16);

      /* local precisionBound2 = M.EPSILON * (sumSquaredNorm + 2.0 * math.abs(dotValue)) / (sqDist + M.EPSILON) */
      lua_pushliteral(L,"EPSILON");
      lua_gettable(L,10);
      lua_pushnumber(L,2);
      lua_getfield(L,LUA_ENVIRONINDEX,"math");
      lua_pushliteral(L,"abs");
      lua_gettable(L,-2);
      lua_remove(L,-2);
      lua_pushvalue(L,16);
      lua_call(L,1,1);
      lc_mul(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lc_add(L,7,-1);
      lua_remove(L,-2);
      lc_mul(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      lua_pushliteral(L,"EPSILON");
      lua_gettable(L,10);
      lc_add(L,9,-1);
      lua_remove(L,-2);
      lc_div(L,-2,-1);
      lua_remove(L,-2);
      lua_remove(L,-2);
      assert(lua_gettop(L) == 17);

      /* if precisionBound2 > precision then */
      enum { lc9 = 17 };
      const int lc10 = lua_lessthan(L,5,17);
      lua_pushboolean(L,lc10);
      const int lc11 = lua_toboolean(L,-1);
      lua_pop(L,1);
      if (lc11) {

        /* sqDist = Vectors.sqdist(v1, v2) */
        lua_pushliteral(L,"sqdist");
        lua_gettable(L,14);
        lua_pushvalue(L,1);
        lua_pushvalue(L,3);
        lua_call(L,2,1);
        lua_replace(L,9);
        assert(lua_gettop(L) == 17);
      }
      lua_settop(L,lc9);
      assert(lua_gettop(L) == 17);
    }
    else {

      /* else
       * sqDist = Vectors.sqdist(v1, v2) */
      lua_pushliteral(L,"sqdist");
      lua_gettable(L,14);
      lua_pushvalue(L,1);
      lua_pushvalue(L,3);
      lua_call(L,2,1);
      lua_replace(L,9);
      assert(lua_gettop(L) == 15);
    }
    lua_settop(L,lc7);
  }
  lua_settop(L,lc4);
  assert(lua_gettop(L) == 15);

  /* return sqDist */
  lua_pushvalue(L,9);
  return 1;
  assert(lua_gettop(L) == 15);
}


// ============================================================================
// Definition of the module Romtable.
// ============================================================================

const LUA_REG_TYPE stuartml_utilml_map[] = {
  { LSTRKEY("EPSILON")            , LRO_NUMVAL(2.2204460492503e-16) },
  { LSTRKEY("fastSquaredDistance"), LFUNCVAL  (stuartml_util_fastSquaredDistance) },
  { LNILKEY, LNILVAL }
};


// ============================================================================
// Registration of the module.
// ============================================================================
LUALIB_API int luaopen_stuartml_utilml( lua_State *L ){
#if LUA_OPTIMIZE_MEMORY > 0
  return 0;
#else
  LREGISTER( L, AUXLIB_STUARTML_UTILML, stuartml_utilml_map );
  return 1;
#endif
}
