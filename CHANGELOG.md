## [Unreleased]
### Added
- [#31](https://github.com/BixData/stuart-ml/issues/31) `[linalg:]` Support `DenseMatrix`, `Matrices`, `Matrix`, and `SparseMatrix` modules
- `DenseMatrix` and `SparseMatrix` map() and update() support
- `DenseMatrix` diag() and eye() support

## [2.0.0-0.2] - 2018-12-24
### Changed
- C source file renames within stuart-elua so that automation can derive a source filename from the rock package

## [2.0.0-0.1] - 2018-12-23
### Changed
- C source file renames so that automation can derive a source filename from the rock package

## [2.0.0-0.0] - 2018-12-22
### Changed
- Merge changes since Stuart 2.0.0-0

## [1.0.1-0.0] - 2018-11-17
### Added
- [linalg] Transpile modules to C: `BLAS`, `DenseVector`, `SparseVector`, `util`, `Vector`, and `Vectors`
- [clustering] Transpile modules to C: `KMeans` (existing model evaluation only), and `VectorWithNorm`
- [util] Transpile module to C: `MLUtils`

<small>(formatted per [keepachangelog-1.1.0](http://keepachangelog.com/en/1.0.0/))</small>
