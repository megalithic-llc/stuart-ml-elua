<img align="right" src="stuart.png" width="70">

## Stuart ML on eLua

[Stuart ML](https://github.com/BixData/stuart-ml) modules transpiled to C for [eLua](https://github.com/elua/elua).

Because digital transformation is driving the need for embedded real-time intelligence to inform control loops at the device edge.

## Building

```sh
$ make

...
*********************************
Compiling eLua ...
CPU:            	STM32F103ZE
Board:          	stm3210e-eval
Platform:       	stm32
Allocator:      	newlib
Boot Mode:      	standard
Target:         	lua
Toolchain:      	codesourcery
ROMFS mode:     	verbatim
Version:        	v0.9-349-g2e6a3af
*********************************
...
Step 60/61 : VOLUME /export
 ---> Running in 70ad2a5643c4
Removing intermediate container 70ad2a5643c4
 ---> a855f67415b5
Step 61/61 : CMD cp -v elua_lua* /export
 ---> Running in 7afe8167847a
Removing intermediate container 7afe8167847a
 ---> d57979f562a8
Successfully built d57979f562a8
Successfully tagged elua:latest
docker run -v `pwd`:/export -it elua
'elua_lua_stm3210e-eval.bin' -> '/export/elua_lua_stm3210e-eval.bin'
'elua_lua_stm3210e-eval.elf' -> '/export/elua_lua_stm3210e-eval.elf'
'elua_lua_stm3210e-eval.hex' -> '/export/elua_lua_stm3210e-eval.hex'
```

Deploy:

```sh
$ stm32flash -w elua_lua_stm3210e-eval.bin /dev/tty.usbserial 

stm32flash 0.4

http://stm32flash.googlecode.com/

Using Parser : Raw BINARY
Interface serial_posix: 57600 8E1
Version      : 0x30
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0430 (XL-density)
- RAM        : 96KiB  (2048b reserved by bootloader)
- Flash      : 1024KiB (sector size: 2x2048)
- Option RAM : 16b
- System RAM : 6KiB
Write to memory
Erasing memory
Wrote address 0x08064340 (100.00%) Done.
```

## Running

Connect a terminal:

```sh
$ screen /dev/tty.usbserial 115200,cs8,-parenb,-cstopb,-hupclcd .
```

View the ROM fs, which contains many of Stuart's test suites:

```sh
eLua# ls

/wo

Total on /wo: 0 bytes

/rom
data/mllib/kmeans_data.txt     72 bytes
examples/SparkPi.luo           556 bytes
test/ml/Matrices.luo           8492 bytes
test/ml/KMeans.luo             1492 bytes
test/ml/BLAS.luo               1508 bytes
test/ml/MLUtils.luo            3520 bytes
test/ml/Vectors.luo            5992 bytes
test/ml/VectorWithNorm.luo     2472 bytes

Total on /rom: 24104 bytes
```

Run a test suite:

```sh
eLua# lua /rom/test/ml/Vectors.luo

Press CTRL+Z to exit Lua
Begin test
✓ dense vector construction with varargs
✓ sparse vector construction
✓ sparse vector construction with unordered elements
✓ sparse vector construction with mismatched indices/values array
✓ sparse vector construction with too many indices vs size
✓ dense to array
✓ dense argmax
✓ sparse to array
✓ sparse argmax
✖ vector equals
  FAILED: attempt to index a nil value
✖ vectors equals with explicit 0
  FAILED: attempt to index a nil value
✓ indexing dense vectors
✓ indexing sparse vectors
End of test: 2 failures
```

Run a "Spark Shell":

```sh
eLua# lua

Press CTRL+Z to exit Lua
Lua 5.1.4  Copyright (C) 1994-2011 Lua.org, PUC-Rio

> Vectors = require 'stuart-ml.linalg.Vectors'
> myDenseVector = Vectors.dense({1,2,3,4})
> mySparseVector = Vectors.sparse(3, {1,2,3,4}, {3.0,5.0,7.0,9.0})
>
> VectorWithNorm = require 'stuart-ml.clustering.VectorWithNorm'
> KMeans = require 'stuart-ml.clustering.KMeans'
> centers = {VectorWithNorm.new(Vectors.dense(1,2,6))}
> point = VectorWithNorm.new(Vectors.dense(1,2,6))
> bestIndex, bestDistance = KMeans.findClosest(centers, point)
```
